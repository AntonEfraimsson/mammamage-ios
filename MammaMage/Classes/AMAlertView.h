//
//  AMAlertView.h
//  PTO
//
//  Created by Artur Mkrtchyan on 3/19/14.
//  Copyright (c) 2014 SocialObjects Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMAlertView : NSObject<UIAlertViewDelegate>

@property (strong, nonatomic) UIAlertView *alertView;

+ (id)initWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)cancelButtonTitle cancelButtonTitle:(NSString *)okButtonTitle completedHandler:(void (^)(BOOL result))callback;

- (void)show;

@end