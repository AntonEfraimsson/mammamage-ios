//
//  AMAlertView.m
//  PTO
//
//  Created by Artur Mkrtchyan on 3/19/14.
//  Copyright (c) 2014 SocialObjects Software. All rights reserved.
//

#import "AMAlertView.h"

@interface AMAlertView()

@property (copy) void (^callback) (BOOL);
@end

static AMAlertView *alert;
@implementation AMAlertView

+ (AMAlertView *)initWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)cancelButtonTitle cancelButtonTitle:(NSString *)okButtonTitle completedHandler:(void (^)(BOOL result))callback
{
    alert = [[AMAlertView alloc] init];
    if (self)
    {
        alert.alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:alert cancelButtonTitle:cancelButtonTitle otherButtonTitles:okButtonTitle, nil];
        
        alert.callback = callback;
    }
    
    return alert;
}

- (void)show
{
    [self.alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        if (self.callback)
            self.callback(YES);
    }
    else
    {
        if (self.callback)
            self.callback(NO);
    }
}

@end