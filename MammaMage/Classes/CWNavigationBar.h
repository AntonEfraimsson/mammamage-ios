//
//  CWNavigationBar.h
//  Catwalk
//
//  Created by Artur Mkrtchyan on 1/8/14.
//  Copyright (c) 2014 Artur Mkrtchyan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CWNavigationBar : UINavigationBar

@property (strong, nonatomic) UIButton *hhButton;
@property (strong, nonatomic) UIBarButtonItem *backButton;

@end
