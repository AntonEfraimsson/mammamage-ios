//
//  CWNavigationBar.m
//  Catwalk
//
//  Created by Artur Mkrtchyan on 1/8/14.
//  Copyright (c) 2014 Artur Mkrtchyan. All rights reserved.
//

#import "CWNavigationBar.h"

@interface CWNavigationBar()

@property (strong, nonatomic) UIButton *backButtonCustomView;


@end

@implementation CWNavigationBar


- (id)init
{
    self = [super init];
    if (self)
    {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setup];
}

- (void)setup
{
    self.backButtonCustomView = [UIButton buttonWithType:UIButtonTypeCustom];
    self.backButtonCustomView.backgroundColor = [UIColor clearColor];
    
    [self.backButtonCustomView setImage:[UIImage imageNamed:@"btn_back_normal.png"] forState:UIControlStateNormal];
    [self.backButtonCustomView setImage:[UIImage imageNamed:@"btn_back_highlight.png"] forState:UIControlStateHighlighted];
    
    self.backButtonCustomView.frame = CGRectMake(0, 0, 26*2, self.frame.size.height);
    [self.backButtonCustomView addTarget:self action:@selector(handleBackButton:) forControlEvents:UIControlEventTouchUpInside];
    
    self.backButton = [[UIBarButtonItem alloc] initWithCustomView: self.backButtonCustomView];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if ([[self navigationController] viewControllers].count > 1)
        [self.topItem setLeftBarButtonItem:self.backButton animated:NO];
}

- (void)handleBackButton:(id)sender
{
    UINavigationController *nvc = [self navigationController];
    [nvc popViewControllerAnimated:YES];
}

- (UINavigationController*)navigationController
{
    UINavigationController *resultNVC = nil;
    UIViewController *vc = nil;
    for (UIView* next = [self superview]; next; next = next.superview)
    {
        UIResponder* nextResponder = [next nextResponder];
        
        if ([nextResponder isKindOfClass:[UIViewController class]])
        {
            vc = (UIViewController*)nextResponder;
            break;
        }
    }
    
    if (vc)
    {
        if ([vc isKindOfClass:[UINavigationController class]])
        {
            resultNVC = (UINavigationController *)vc;
        }
        else
        {
            resultNVC = vc.navigationController;
        }
    }
    
    return resultNVC;
}


@end
