//
//  Constants.h
//  PTPlus
//
//  Created by Ashot Tonoyan on 28/05/2012.
//  Copyright (c) 2012 SocialObjects Software. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DB @"MammaMage.sqlite"

#define FONT_NORMAL @"GillSans"
#define FONT_BOLD @"GillSansStd-Bold"
#define FONT_ITALIC @"GillSans-Italic"

#define TEXT_COLOR @"#454545"

#define TIMER_STATE_UNKNOWN 0
#define TIMER_STATE_EXERCISE 1
#define TIMER_STATE_PAUSE 2

#define IMAGE_HEIGHT 214


#define LOCK_ALL_LEVELS @"YES"