//
//  CustomMoviePlayerVC.h
//  MammaMage
//
//  Created by Narek Haytyan on 10/30/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface CustomMoviePlayerVC : UIViewController {
    MPMoviePlayerController *mp;
    NSURL *movieURL;
}

- (id)initWithPath:(NSString *)moviePath tag:(int)tag;

- (void)startPlayer;
- (void)pausePlayer;
- (void)stopPlayer;
- (UIImage *)getVideoThumbnail;
- (MPMoviePlaybackState)getVideoPlaybackState;
- (MPMovieLoadState)getVideoLoadState;
- (void)enterFullScreenAnimated:(BOOL)animated;
- (void)exitFullScreenAnimated:(BOOL)animated;


- (void)prepare;

@end
