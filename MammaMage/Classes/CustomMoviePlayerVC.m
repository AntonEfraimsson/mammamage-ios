//
//  CustomMoviePlayerVC.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/30/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "CustomMoviePlayerVC.h"

@interface CustomMoviePlayerVC ()

@end

@implementation CustomMoviePlayerVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithPath:(NSString *)moviePath tag:(int)tag
{
    // Initialize and create movie URL
    if (self = [super init])
    {
        movieURL = [NSURL fileURLWithPath:moviePath];
        
        mp =  [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
        
        // Set movie player layout
        [mp setShouldAutoplay:NO];
        [mp setFullscreen:NO];
        
        // May help to reduce latency
        [mp prepareToPlay];
        
        // Register that the load state changed (movie is ready)
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlayerLoadStateChanged:)
                                                     name:MPMoviePlayerPlaybackStateDidChangeNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector: @selector(MoviePlayerThumbnailImageRequestDidFinish:)
                                              name: MPMoviePlayerThumbnailImageRequestDidFinishNotification
                                                   object: nil];
        
        if(tag == 1) {
            [mp setControlStyle:MPMovieControlStyleEmbedded];
        }

    }
    return self;
}

- (void)MoviePlayerThumbnailImageRequestDidFinish:(NSNotification*)not
{
    
}

- (void)enterFullScreenAnimated:(BOOL)animated {
    [mp setFullscreen:YES animated:animated];
}

- (void)exitFullScreenAnimated:(BOOL)animated {
    [mp setFullscreen:NO animated:animated];
}

- (void) moviePlayerLoadStateChanged:(NSNotification *)notification{
    
    // Unless state is unknown, start playback
    if ([mp loadState] != MPMovieLoadStateUnknown)
    {
//        [[self view] setBounds:CGRectMake(0, 0, 320, 240)];
//        [[self view] setCenter:CGPointMake(160, 120)];
        
        // Set frame of movie player
        [[mp view] setFrame:self.view.frame];
        
        // Add movie player as subview
        [[self view] addSubview:[mp view]];
        

    }
}

- (void) startPlayer {
    
    if(mp.loadState != MPMovieLoadStateUnknown) {
        [mp play];
    }
}

- (void) pausePlayer {
    
    if(mp.loadState != MPMovieLoadStateUnknown) {
        [mp pause];
    }
}

- (void) stopPlayer {
    
    if(mp.loadState != MPMovieLoadStateUnknown) {
        [mp stop];
    }
}


- (UIImage *)getVideoThumbnail {

    if (mp && mp.loadState != MPMovieLoadStateUnknown) {
        return [mp thumbnailImageAtTime:0.3 timeOption:MPMovieTimeOptionExact];
    }
    
    return nil;
}

- (MPMoviePlaybackState)getVideoPlaybackState
{
    return mp.playbackState;
}

- (MPMovieLoadState)getVideoLoadState
{
    return mp.loadState;
}

- (void)prepare
{
    [mp prepareToPlay];
}

@end
