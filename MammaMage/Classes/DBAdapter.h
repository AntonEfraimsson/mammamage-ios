//
//  DBAdapter.h
//  PTPlus
//
//  Created by Ashot Tonoyan on 28/05/2012.
//  Copyright (c) 2012 SocialObjects Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EGODatabase.h"

@interface DBAdapter : NSObject

@property (strong, nonatomic) EGODatabase* database;
@property (readonly, nonatomic) NSNumber *lastInsertId;
@property (nonatomic) NSString *tableExercise;
@property (nonatomic) NSString *tableLevels;

#pragma mark - Database migration
- (void)checkForDatabaseMigration;

#pragma mark - Transaction
- (EGODatabaseResult *)startTransaction;
- (EGODatabaseResult *)commit;
- (EGODatabaseResult *)rollback;

+ (DBAdapter *)sharedInstance;

@end
