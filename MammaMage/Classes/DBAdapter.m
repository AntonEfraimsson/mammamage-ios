//
//  DBAdapter.m
//  PTPlus
//
//  Created by Ashot Tonoyan on 28/05/2012.
//  Copyright (c) 2012 SocialObjects Software. All rights reserved.
//

#import "DBAdapter.h"


@implementation DBAdapter

#define DB_VERSION 1.94

#pragma mark - Tadabase migration
- (void)checkForDatabaseMigration
{
    NSNumber *db_version = [[NSUserDefaults standardUserDefaults] objectForKey:@"db_version"];
    if (!db_version || (int)([db_version floatValue] * 100) < (int)(DB_VERSION * 100)) //If db version on device is old, then replace with new one
    {
        [self migrateDatabaseIfNecessary];
    }
}

- (void)migrateDatabaseIfNecessary
{
    EGODatabase *database = self.database;
    
    // Selecting data from current database for moving into new database
    EGODatabaseResult *levelResult = [database executeQuery:@"SELECT * FROM `level`"];
    EGODatabaseResult *exerciseResult = [database executeQuery:@"SELECT * FROM `exercise`"];
    
    [Utils createEditableCopyOfResourceForce:DB];
    _database = nil;
    database = self.database;

    //Insert back exercise user's data
    for (EGODatabaseRow *row in exerciseResult) {
        int ex_id = [row intForColumn:@"id"];
        int completed_steps = [row intForColumn:@"completed_steps"];
        int required_steps = [row intForColumn:@"required_steps"];
        int completed = [row intForColumn:@"completed"];
        int completed_count = [row intForColumn:@"completed_count"];
//        [database executeQuery:query];
        [database executeUpdateWithParameters:@"UPDATE `exercise` SET `completed_steps`=?, `required_steps`=?, `completed`=?,  `completed_count`=? WHERE `id`=?", @(completed_steps), @(required_steps), @(completed), @(completed_count), @(ex_id), nil];
    }
    
    //Insert back levels user's data
    for (EGODatabaseRow *row in levelResult) {
        int lvl_id = [row intForColumn:@"id"];
        int last_step_time = [row intForColumn:@"last_step_time"];
        int required_steps = [row intForColumn:@"required_steps"];
        int completed = [row intForColumn:@"completed"];
        int accessible = [row intForColumn:@"accessible"];
        [database executeUpdateWithParameters:@"UPDATE `level` SET `last_step_time`=?, `required_steps`=?, `completed`=?, `accessible`=? WHERE `id`=?", @(last_step_time), @(required_steps), @(completed), @(accessible), @(lvl_id), nil];
    }

    
    // Moving selected data to new database
//    [self insertOrUpdateTable:@"level" databaseResult:levelResult database:database];
//    [self insertOrUpdateTable:@"exercise" databaseResult:exerciseResult database:database];
    
    //Update DB version to latest
    [[NSUserDefaults standardUserDefaults] setObject:@(DB_VERSION) forKey:@"db_version"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)insertOrUpdateTable:(NSString *)table databaseResult:(EGODatabaseResult *)result database:(EGODatabase *) database
{
    [database executeQuery:[NSString stringWithFormat:@"DELETE FROM `%@`", table]];
    
    for (EGODatabaseRow *row in result) {
        
        NSString *query = [NSString stringWithFormat:@"INSERT OR REPLACE INTO `%@` (",table];
        
        for (NSString *column in result.columnNames) {
            query = [query stringByAppendingFormat:@"`%@`,",column];
        }
        
        query = [query substringToIndex:[query length] - 1]; //removing last ',' char from the end
        query = [query stringByAppendingString:@") VALUES ("];
        
        
        for (NSString *column in result.columnNames) {
            NSData *data = [row dataForColumn:column];
            
            query = [query stringByAppendingFormat:@"'%@',",data];
        }

        query = [query substringToIndex:[query length] - 1]; //removing last ',' char from the end
        query = [query stringByAppendingString:@");"];
        
        
        [database executeQuery:query];
    }
}

#pragma mark -
+ (DBAdapter *)sharedInstance
{
    static DBAdapter *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DBAdapter alloc] init];
    });
    return sharedInstance;
}


- (NSString *)dbPath {
    [Utils createEditableCopyOfResourceIfNeeded:DB];
    return [NSHomeDirectory() stringByAppendingPathComponent:[@"Documents/" stringByAppendingString:DB]];
}

- (EGODatabase *)database {
    if (!_database) {
        _database = [EGODatabase databaseWithPath:[self dbPath]];
    }
    return _database;
}

- (NSNumber *)lastInsertId {
    EGODatabase *db = self.database;
    EGODatabaseRow *row = [[db executeQuery:@"SELECT last_insert_rowid() AS `last_insert_id`;"] rowAtIndex:0];
    return [row numberForColumn:@"last_insert_id"];
}


#pragma mark - Transaction

- (EGODatabaseResult *)startTransaction {
    EGODatabase *db = self.database;
    return [db executeQuery:@"START TRANSACTION;"];
}

- (EGODatabaseResult *)commit {
    EGODatabase *db = self.database;
    return [db executeQuery:@"COMMIT;"];
}

- (EGODatabaseResult *)rollback {
    EGODatabase *db = self.database;
    return [db executeQuery:@"ROLLBACK;"];
}

@end
