//
//  DBExercise.h
//  MammaMage
//
//  Created by Narek Haytyan on 10/29/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "DBAdapter.h"

@interface DBExercise : DBAdapter

+ (DBExercise *)sharedInstance;

- (EGODatabaseResult *)selectExercises:(NSNumber *) exerciseId;
- (EGODatabaseResult *)selectExercisesForLevel:(NSNumber *) levelId;
- (EGODatabaseResult *)selectTestExerciseForLevel:(NSNumber *) levelId;
- (void)completeExerciseWithId:(NSNumber *)exerciseId;
- (void)finishExerciseStepWithId:(NSNumber *)exerciseId;
- (void)cancelExerciseWithId:(NSNumber *)exerciseId;

@end
