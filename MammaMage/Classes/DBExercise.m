//
//  DBExercise.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/29/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "DBExercise.h"

@interface DBExercise()

@end

@implementation DBExercise

+ (DBExercise *)sharedInstance
{
    static DBExercise *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DBExercise alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    return self;
}

- (EGODatabaseResult *)selectExercises:(NSNumber *)exerciseId {
    EGODatabase *db = self.database;
    EGODatabaseResult *result;
    if (exerciseId) {
        result = [db executeQueryWithParameters:@" \
                  SELECT * \
                  FROM `exercise` \
                  WHERE `id` = ? AND `skip` = 0", exerciseId, nil];
    } else {
        result = [db executeQueryWithParameters:@" \
                  SELECT * \
                  FROM `exercise`;", nil];
    }
    return result;
}

- (EGODatabaseResult *)selectExercisesForLevel:(NSNumber *) levelId {
    EGODatabase *db = self.database;
    EGODatabaseResult *result;
    if (levelId) {
        result = [db executeQueryWithParameters:@" \
                  SELECT * \
                  FROM `exercise` \
                  WHERE `level_id` = ? AND `skip` = 0 AND `is_test` <> 1 \
                  ORDER BY `name`;", levelId, nil];
    }
    return result;
}

- (EGODatabaseResult *)selectTestExerciseForLevel:(NSNumber *) levelId {
    EGODatabase *db = self.database;
    EGODatabaseResult *result;
    if (levelId) {
        result = [db executeQueryWithParameters:@" \
                  SELECT * \
                  FROM `exercise` \
                  WHERE `level_id` = ? AND `is_test` = 1 AND `skip` = 0;", levelId, nil];
    }
    return result;
}

- (void)completeExerciseWithId:(NSNumber *)exerciseId {
    EGODatabase *db = self.database;
    if(exerciseId) {
        EGODatabaseResult *result = [db executeQueryWithParameters:@"SELECT * FROM `exercise` WHERE `id`=? AND `skip` = 0",
                                     exerciseId, nil];
        NSNumber *completed_count = @(0);
        if (result && [result count])
        {
            completed_count = [[result rowAtIndex:0] numberForColumn:@"completed_count"];
            if (completed_count)
            {
                completed_count = @([completed_count intValue] + 1);
            }
        }
        
        [db executeUpdateWithParameters:@"\
         UPDATE `exercise` \
         SET `completed` = 1, `completed_steps` = 0 \, `completed_count` = ?\
         WHERE `id`=?;", completed_count, exerciseId, nil];
    }
}

- (void)finishExerciseStepWithId:(NSNumber *)exerciseId {
    EGODatabase *db = self.database;
    if(exerciseId) {
        [db executeUpdateWithParameters:@"\
         UPDATE `exercise` \
         SET `completed_steps`=`completed_steps`+1 \
         WHERE `id`=?;", exerciseId, nil];
    }
}

- (void)cancelExerciseWithId:(NSNumber *)exerciseId {
    EGODatabase *db = self.database;
    if(exerciseId) {
        [db executeUpdateWithParameters:@"\
         UPDATE `exercise` \
         SET `completed_steps`=0, `completed` = 0 \
         WHERE `id`=?;", exerciseId, nil];
    }
}

@end
