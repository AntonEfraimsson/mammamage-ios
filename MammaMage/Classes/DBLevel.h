//
//  DBLevel.h
//  MammaMage
//
//  Created by Narek Haytyan on 10/27/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "DBAdapter.h"

@interface DBLevel : DBAdapter

+ (DBLevel *)sharedInstance;

- (EGODatabaseResult *)selectLevels:(NSNumber *) levelId;
- (BOOL)completeLevelWithId:(NSNumber *)levelId;
- (BOOL)makeAccessibleLevelWithId:(NSNumber *)levelId;
- (NSNumber *)getRemainingStepsCountForLevelId:(NSNumber *)levelId;
- (NSNumber *)getTotalCompletedExercisesCountForLevelId:(NSNumber *)levelId;
- (NSInteger)getLastStepTimeForLevelId:(NSNumber *)levelId;
- (NSString *)getMotivatingTextForLevelId:(NSNumber *)levelId;
- (BOOL)completeLevelStepWithId:(NSNumber *)levelId;

@end
