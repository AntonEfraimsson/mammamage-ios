//
//  DBLevel.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/27/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "DBLevel.h"

@interface DBLevel()

@end

@implementation DBLevel

+ (DBLevel *)sharedInstance
{
    static DBLevel *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DBLevel alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    return self;
}

- (EGODatabaseResult *)selectLevels:(NSNumber *)levelId {
    EGODatabase *db = self.database;
    EGODatabaseResult *result;
    if (levelId) {
        result = [db executeQueryWithParameters:@" \
                  SELECT * \
                  FROM `level` \
                  WHERE `id` = ?;", levelId, nil];
    } else {
        result = [db executeQueryWithParameters:@" \
                  SELECT * \
                  FROM `level`;", nil];
    }
    return result;
}

- (BOOL)completeLevelWithId:(NSNumber *)levelId {
    EGODatabase *db = self.database;
    
    return [db executeUpdateWithParameters:@" \
            UPDATE `level` \
            SET `completed`=1 \
            WHERE `id`=?", levelId, nil];
}

- (BOOL)makeAccessibleLevelWithId:(NSNumber *)levelId {
    EGODatabase *db = self.database;
    return [db executeUpdateWithParameters:@" \
            UPDATE `level` \
            SET `accessible`=1 \
            WHERE `id`=?", levelId, nil];
}

- (NSNumber *)getRemainingStepsCountForLevelId:(NSNumber *)levelId {
    EGODatabase *db = self.database;
    EGODatabaseResult *result = [db executeQueryWithParameters:@" \
                                 SELECT `required_steps` \
                                 FROM `level` \
                                 WHERE `id`=?",
                                 levelId, nil];
    
    if (result && result.count > 0) {
        return [[result rowAtIndex:0] numberForColumn:@"required_steps"];
    }
    
    return 0;
}

- (NSNumber *)getTotalCompletedExercisesCountForLevelId:(NSNumber *)levelId {
    EGODatabase *db = self.database;
    EGODatabaseResult *result = [db executeQueryWithParameters:@"SELECT SUM(`completed_count`) as `total_completed_count`\
                                 FROM `exercise` WHERE `level_id` = ?",
                                 levelId, nil];
    
    if (result && result.count > 0) {
        return [[result rowAtIndex:0] numberForColumn:@"total_completed_count"];
    }
    
    return 0;
}

- (NSString *)getMotivatingTextForLevelId:(NSNumber *)levelId{
    EGODatabase *db = self.database;
    EGODatabaseResult *result = [db executeQueryWithParameters:[NSString stringWithFormat:@"SELECT `%@`\
                                 FROM `level` WHERE `id` = ?", NSLocalizedString(@"motivated_text", nil)],
                                 levelId, nil];
    
    if (result && result.count > 0) {
        return [[result rowAtIndex:0] stringForColumn:NSLocalizedString(@"motivated_text", nil)];
    }
    
    return nil;
}

- (NSInteger)getLastStepTimeForLevelId:(NSNumber *)levelId {
    EGODatabase *db = self.database;
    EGODatabaseResult *result = [db executeQueryWithParameters:@" \
                                 SELECT `last_step_time` \
                                 FROM `level` \
                                 WHERE `id`=?",
                                 levelId, nil];
    
    if (result && result.count > 0) {
        NSInteger timestamp = [[result rowAtIndex:0] intForColumn:@"last_step_time"];
        return timestamp;
    }
    
    return 0;
}

- (BOOL)completeLevelStepWithId:(NSNumber *)levelId {
    EGODatabase *db = self.database;
    NSNumber *now = @((int)[[NSDate date] timeIntervalSince1970]);
    return [db executeUpdateWithParameters:@" \
            UPDATE `level` \
            SET `required_steps`=`required_steps`-1, `last_step_time`=? \
            WHERE `id`=?", now, levelId, nil];
}

@end
