//
//  ExerciseTVC.h
//  MammaMage
//
//  Created by Narek Haytyan on 10/29/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExcerciseVC : UITableViewController <UIScrollViewDelegate>

- (void) didFinishTestToggle : (BOOL) answer;

@property (nonatomic) NSNumber *levelId;
@property (nonatomic) NSNumber *exerciseId;
@property (nonatomic) BOOL isTest;

@end
