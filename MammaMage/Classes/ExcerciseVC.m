//
//  ExerciseTVC.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/29/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "ExcerciseVC.h"
#import "CustomMoviePlayerVC.h"
#import "DBExercise.h"
#import "DBLevel.h"
#import "ExerciseProgressView.h"
#import "Utils.h"
#import "ToggleViewController.h"
#import "ToggleViewControllerNiva.h"
#import "ToggleViewControllerTest.h"
#import "ToggleViewControllerDone.h"
#import "DDPageControl.h"
#import "NSAttributedString+Attributes.h"
#import "OHAttributedLabel.h"
#import <AVFoundation/AVFoundation.h>

NSDate *startDate;
NSInteger timerSeconds;
NSTimer *globalTimer;
NSMutableArray *timerStorage;
NSInteger currentState;

@class ToggleViewControllerTest;
@interface ExcerciseVC () <AVAudioPlayerDelegate>

@property (strong, nonatomic)  AVAudioPlayer *beepPlayer;
@property (strong, nonatomic)  AVAudioPlayer *beepBeginingPlayer;
@property (strong, nonatomic)  AVAudioPlayer *beepEndingPlayer;
@property (strong, nonatomic) DBExercise *exerciseDB;
@property (strong, nonatomic) DBLevel *levelDB;
@property (strong, nonatomic) EGODatabaseResult *exercises;
@property (strong, nonatomic) EGODatabaseRow *exercise;

@property (weak, nonatomic) IBOutlet UIView *videoCell;
@property (weak, nonatomic) IBOutlet UIButton *playVideoButton;
@property (strong, nonatomic) CustomMoviePlayerVC *videoPlayer;
@property (weak, nonatomic) IBOutlet UIImageView *videoThumbnail;
@property (nonatomic) BOOL isVideoFullscreen;

@property (weak, nonatomic) IBOutlet UILabel *exerciseCaptionLabel;
@property (weak, nonatomic) IBOutlet OHAttributedLabel *exerciseDescriptionLabel;

@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIView *cancelButtonView;
@property (weak, nonatomic) IBOutlet UILabel *cancelButtonCaptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *cancelButtonTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *cancelButtonCountdownLabel;
@property (weak, nonatomic) IBOutlet ExerciseProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UILabel *pauseButtonTextLabel;

@property (weak, nonatomic) IBOutlet UIView *exerciseDetailsView;

// timer outlets
@property (weak, nonatomic) IBOutlet UIView *timerDetailsView;
@property (weak, nonatomic) IBOutlet UIImageView *timerIcon;
@property (weak, nonatomic) IBOutlet UIImageView *timerIconInactive;
@property (weak, nonatomic) IBOutlet UILabel *timerSecondsLabel;

// delay outlets
@property (weak, nonatomic) IBOutlet UIView *delayDetailsView;
@property (weak, nonatomic) IBOutlet UIImageView *delayIcon;
@property (weak, nonatomic) IBOutlet UIImageView *delayIconInactive;
@property (weak, nonatomic) IBOutlet UILabel *delaySecondsLabel;

// repeat outlets
@property (weak, nonatomic) IBOutlet UIView *repeatDetailsView;
@property (weak, nonatomic) IBOutlet UIImageView *repeatIcon;
@property (weak, nonatomic) IBOutlet UIImageView *repeatIconInactive;
@property (weak, nonatomic) IBOutlet UILabel *repeatNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *repeatNumberXLabel;

@property (nonatomic, strong) ToggleViewController *alertVC;
@property (nonatomic, strong) ToggleViewControllerTest *TestToggleVC;
@property (nonatomic, strong) ToggleViewControllerDone *testToggleDone;
@property (nonatomic, strong) ToggleViewControllerDone *exerciseToggleDone;

@property (weak, nonatomic) IBOutlet UIScrollView *imageScroll;
@property (nonatomic, weak) IBOutlet DDPageControl *pageControl;

@property (nonatomic) BOOL yetToggled;

- (IBAction)cancelButtonPressed:(UIButton *) sender;
@end

@implementation ExcerciseVC

- (void)toggle:(NSNotification *)passport
{
    if ([passport object] == nil) {
        if (APP_DELEGATE.didNotified)
            return;
    }
    APP_DELEGATE.didNotified = YES;
    self.view.userInteractionEnabled=NO;
    ToggleViewControllerNiva *toggleVC = [[ToggleViewControllerNiva alloc] initWithNibName:@"ToggleViewControllerNiva" bundle:[NSBundle mainBundle]];        
    [APP_DELEGATE.window addSubview:toggleVC.view];
    [self addChildViewController:toggleVC];
    
    [UIView animateWithDuration:0.3 animations:^{
        toggleVC.view.alpha = 1;
    }];
    
}

- (IBAction)paged:(DDPageControl *)sender
{
    int resulution = [UIScreen mainScreen].bounds.size.width;
    int page =  sender.currentPage;
    CGPoint offset = self.imageScroll.contentOffset;
    
    offset.x = page * resulution;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.self.imageScroll.contentOffset = offset;
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadData];
    
    NSString *fileName = [[NSBundle mainBundle] pathForResource:@"beep_OneSecond" ofType:@"wav"];
    self.beepPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:fileName] error:NULL];
    self.beepPlayer.delegate = self;
    self.beepPlayer.numberOfLoops = 2;
    [self.beepPlayer prepareToPlay];

    NSString *fileName2 = [[NSBundle mainBundle] pathForResource:@"beep_begining" ofType:@"wav"];
    self.beepBeginingPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:fileName2] error:NULL];
    
    
    
    
    NSString *fileName3 = [[NSBundle mainBundle] pathForResource:@"beep_end" ofType:@"wav"];
    self.beepEndingPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:fileName3] error:NULL];
    
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toggle:) name:@"showDialog" object:nil];
    
    self.imageScroll.hidden  = NO;
    
    timerStorage = [[NSMutableArray alloc] init];
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc] initWithTitle:@" "
                                                                style:UIBarButtonItemStylePlain
                                                               target:nil
                                                               action:nil];
    self.navigationItem.backBarButtonItem = backBtn;
    
    self.navigationItem.title = [self.exercise stringForColumn:NSLocalizedString(@"caption", nil)];
    
    self.navigationController.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[@"bg_level" stringByAppendingString:[NSString stringWithFormat:@"%d.jpg", [self.exercise intForColumn:@"level_id"]]]]];
    
    self.exerciseCaptionLabel.font = [UIFont fontWithName:FONT_BOLD size:self.exerciseCaptionLabel.font.pointSize];
    self.exerciseCaptionLabel.text = [self.exercise stringForColumn:NSLocalizedString(@"caption", nil)];
    
    NSString *description = [self.exercise stringForColumn:NSLocalizedString(@"description", nil)];
    CGRect frame = self.exerciseDescriptionLabel.frame;
    CGFloat oldHeight = frame.size.height;
    CGFloat newHeight = [description sizeWithFont:self.exerciseDescriptionLabel.font
                                constrainedToSize:CGSizeMake(frame.size.width, 999)
                                    lineBreakMode:UILineBreakModeWordWrap].height;
    //    newHeight -= newHeight*0.175;
    CGFloat heightDelta = newHeight - oldHeight;
    frame.size.height = newHeight + 50;
    self.exerciseDescriptionLabel.frame = frame;
    
    frame = self.exerciseDetailsView.frame;
    frame.origin.y += heightDelta;
    self.exerciseDetailsView.frame = frame;
    
    frame = self.exerciseDetailsView.superview.frame;
    frame.size.height = frame.size.height + heightDelta - 60;
    self.exerciseDetailsView.superview.frame = frame;
    
    //Custom initing label *L0gg3r*
    NSMutableAttributedString *bufferStr;
    UIFont* bold13 = [UIFont fontWithName:FONT_BOLD size:13];
    bufferStr = [NSMutableAttributedString attributedStringWithString: description];
    [bufferStr setTextColor:[UIColor whiteColor]];
    [bufferStr setFont:[UIFont fontWithName:FONT_NORMAL size:13]];
    
    [bufferStr setFont:bold13 range: [description rangeOfString:NSLocalizedString(@"suggestion", nil)]];
    self.exerciseDescriptionLabel.attributedText = bufferStr;
    
    NSRange range = [description rangeOfString:NSLocalizedString(@"level", nil)];
    
    if(range.length) {
        [self.exerciseDescriptionLabel addCustomLink:[NSURL URLWithString:@"mm82735462871824://"] inRange: NSMakeRange(range.location, range.length + 2)];
        [self.exerciseDescriptionLabel setLinkColor:[UIColor darkTextColor]];
    }
    
    //end
    
    if ([self.exercise intForColumn:@"level_id"] == 4) {
        self.exerciseCaptionLabel.textColor = [UIColor blackColor];
        [self.exerciseDescriptionLabel setTextColor:[UIColor blackColor]];
    }
    
    self.startButton.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:self.startButton.titleLabel.font.pointSize];
    if([self.exercise intForColumn:@"is_test"] == 0) {
        [self.startButton setTitle:NSLocalizedString(@"start_exercise", nil) forState:UIControlStateNormal];
    } else {
        [self.startButton setTitle:NSLocalizedString(@"start_test", nil) forState:UIControlStateNormal];
    }
    self.backButton.titleLabel.font = self.startButton.titleLabel.font;
    
    // initializing progress bar
    self.progressBar.levelNumber = [self.exercise intForColumn:@"level_id"];
    self.progressBar.progress = 0;
    
    // initializing background image
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[@"bg_level" stringByAppendingString:[NSString stringWithFormat:@"%d.jpg", [self.exercise intForColumn:@"level_id"]]]]];
    
    
    
    // initializing icon fonts
    [self initIconFonts];
    
    //initializing cancel button fonts
    self.cancelButtonCaptionLabel.font = self.startButton.titleLabel.font;
    self.cancelButtonCountdownLabel.font = self.cancelButtonCaptionLabel.font;
    self.cancelButtonTextLabel.font = [UIFont fontWithName:FONT_NORMAL size:self.cancelButtonTextLabel.font.pointSize];
    self.cancelButtonCaptionLabel.text = NSLocalizedString(@"timer_start_in", nil);
    self.cancelButtonTextLabel.text = NSLocalizedString(@"timer_pause", nil);
    
    self.pauseButtonTextLabel.font = self.cancelButtonTextLabel.font;
    self.pauseButtonTextLabel.text = NSLocalizedString(@"timer_pause", nil);
    
    self.timerSecondsLabel.text = [NSString stringWithFormat:@"%d %@", [self.exercise intForColumn:@"length"], NSLocalizedString(@"second", nil)];
    self.delaySecondsLabel.text = [NSString stringWithFormat:@"%d %@", [self.exercise intForColumn:@"delay"], NSLocalizedString(@"second", nil)];
    NSInteger stepsToRepeat = [self.exercise intForColumn:@"required_steps"] - [self.exercise intForColumn:@"completed_steps"];
    self.repeatNumberLabel.text = [NSString stringWithFormat:@"%d %@", stepsToRepeat, NSLocalizedString(@"times", nil)];
    self.repeatNumberXLabel.text = [NSString stringWithFormat:@"%dx", stepsToRepeat];
    
    //Popup Screen answer notification
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(toggleDidFinish:)
                                                 name:@"aboutToggleState"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willResignActiveState:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didBecomeActiceState:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];

    int isPhoto = [self.exercise intForColumn:@"is_photo"];
    NSArray *bufferArray = [[NSArray alloc] init];
    NSString *buffer = [[NSString alloc]  init];
    
    
    if (0 != isPhoto) {
        self.videoThumbnail.hidden = YES;
        self.playVideoButton.hidden = YES;
        
        self.imageScroll.hidden = NO;
        self.imageScroll.contentSize = CGSizeMake (isPhoto * 320 ,self.imageScroll.frame.size.height);
        self.imageScroll.frame = CGRectMake(self.imageScroll.frame.origin.x, self.imageScroll.frame.origin.y, [UIScreen mainScreen].bounds.size.width, self.imageScroll.frame.size.height);
        
        self.imageScroll.pagingEnabled = YES;
        //initing pageControl
        [self.pageControl setNumberOfPages:isPhoto];
        [self.pageControl setIndicatorDiameter:7];
        [self.pageControl setOnColor:[UIColor whiteColor]];
        [self.pageControl setOffColor:[UIColor darkGrayColor]];
        self.pageControl.center = CGPointMake( self.view.center.x, self.pageControl.center.y);
        for (int i = 0; i < isPhoto; i++){
            buffer = [self.exercise stringForColumn:@"video_filename"];
            bufferArray = [buffer componentsSeparatedByString:@"/"];
            ((UIImageView *)self.imageScroll.subviews[i]).image = [UIImage imageNamed:(NSString*)bufferArray[i]];
        }
        
        if (1 == isPhoto) {
            self.pageControl.hidden = YES;
        }
        self.videoCell.frame = CGRectMake(0, 0, 320, 235);
    } else {
        [self initVideoPlayer];
        self.videoCell.frame = CGRectMake(0, 0, 320, 180);
        NSLog(@"[[UIDevice currentDevice] systemVersion] floatValue] %f",[[[UIDevice currentDevice] systemVersion] floatValue]);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] < 6.0f) {
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenHeight = screenRect.size.height;
            if (screenHeight <= 480) {
                self.exerciseCaptionLabel.frame = CGRectMake(0, -62, 290, 36);
                self.exerciseDescriptionLabel.frame = CGRectMake(0, -30, self.exerciseDescriptionLabel.frame.size.width, self.exerciseDescriptionLabel.frame.size.height );
                self.exerciseDetailsView.frame = CGRectMake(8, self.exerciseDescriptionLabel.frame.size.height -44 , self.exerciseDetailsView.frame.size.width, self.exerciseDetailsView.frame.size.height );
            }
        }
    }
    if (IS_IOS_7) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    if (!self.yetToggled) {
        [self toggle:nil];
        self.yetToggled = YES;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int resulution = [UIScreen mainScreen].bounds.size.width;
    int page = scrollView.contentOffset.x/resulution;
    [self.pageControl setCurrentPage:page];
}

- (void) initVideoPlayer
{
    self.imageScroll.hidden = YES;
    self.pageControl.hidden = YES;
    
    // initializing media player
    NSString *filename = [[self.exercise stringForColumn:@"video_filename"] stringByDeletingPathExtension];
    NSString *extension = [[self.exercise stringForColumn:@"video_filename"] pathExtension];
    
    NSString *videoPath = [[NSBundle mainBundle]
                           pathForResource:filename
                           ofType:extension];
    if(!self.videoPlayer) {
        self.videoPlayer = [[CustomMoviePlayerVC alloc] initWithPath:videoPath tag:1];
        
        // registering for state change notification
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlayerLoadStateChanged:)
                                                     name:MPMoviePlayerLoadStateDidChangeNotification
                                                   object:nil];
        
        // registering for playback state change notification
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlaybackStateDidChange:)
                                                     name:MPMoviePlayerPlaybackStateDidChangeNotification
                                                   object:nil];
        
        // registering for playback finish notification
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlaybackDidFinish:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:nil];
        
        // registering for playback finish notification
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlaybackWillEnterFullscreen:)
                                                     name:MPMoviePlayerWillEnterFullscreenNotification
                                                   object:nil];
        
        // registering for playback finish notification
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlaybackDidExitFullscreen:)
                                                     name:MPMoviePlayerDidExitFullscreenNotification
                                                   object:nil];
        
        
        self.videoPlayer.view.autoresizingMask = UIViewAutoresizingNone;
        self.videoPlayer.view.frame = CGRectMake(0, 0, self.videoThumbnail.frame.size.width, self.videoThumbnail.frame.size.height);
        [self.videoCell addSubview:self.videoPlayer.view];
        [self.videoPlayer.view setHidden:YES];
    }
}


- (void)loadData
{
    self.exerciseDB = [DBExercise sharedInstance];
    self.levelDB = [DBLevel sharedInstance];
    
    if (!self.isTest) {
        self.exercises = [self.exerciseDB selectExercises:self.exerciseId];
    } else {
        self.exercises = [self.exerciseDB selectTestExerciseForLevel:self.levelId];
    }
    self.exercise = [self.exercises rowAtIndex:0];
    self.exerciseId = [self.exercise numberForColumn:@"id"];
    self.levelId = [self.exercise numberForColumn:@"level_id"];
}

- (void)willResignActiveState:(NSNotification *)notification
{
    [self loadData];
    if([globalTimer isValid]) {
        [self pauseButtonPressed:nil];
    }
}

- (void)didBecomeActiceState:(NSNotification *)notification
{
    [self toggle:nil];
}

- (void)initIconFonts
{
    UIFont *labelFontBold = [UIFont fontWithName:FONT_BOLD size:13];
    UIFont *labelFontSmall = [UIFont fontWithName:FONT_BOLD size:9];
    for(UIView *subview in self.timerDetailsView.subviews) {
        if([subview isKindOfClass:[UILabel class]]) {
            ((UILabel *)subview).font = labelFontBold;
        }
    }
    for(UIView *subview in self.delayDetailsView.subviews) {
        if([subview isKindOfClass:[UILabel class]]) {
            ((UILabel *)subview).font = labelFontBold;
        }
    }
    for(UIView *subview in self.repeatDetailsView.subviews) {
        if([subview isKindOfClass:[UILabel class]]) {
            if(subview.tag == 2) {
                ((UILabel *)subview).font = labelFontSmall;
            } else {
                ((UILabel *)subview).font = labelFontBold;
            }
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    if(!self.isVideoFullscreen) {
        [globalTimer invalidate];
    }
    
    [super viewWillDisappear:animated];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.videoPlayer stopPlayer];
    self.videoPlayer = nil;
    
    self.beepPlayer.delegate = nil;
    self.beepBeginingPlayer.delegate = nil;
    self.beepEndingPlayer.delegate = nil;
    [self.beepBeginingPlayer stop];
    [self.beepEndingPlayer stop];
    [self.beepPlayer stop];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [globalTimer invalidate];
    globalTimer = nil;
}

- (void)moviePlayerLoadStateChanged:(NSNotification *)notification
{
    self.videoThumbnail.image = [self.videoPlayer getVideoThumbnail];
    [self.videoThumbnail setHidden:NO];
}

- (void)moviePlaybackDidFinish:(NSNotification *)notification
{
    [self.videoPlayer exitFullScreenAnimated:YES];
    [self.videoPlayer.view setHidden:YES];
    [self.videoThumbnail setHidden:NO];
}

- (void)moviePlaybackStateDidChange:(NSNotification *)notification
{
    switch ([self.videoPlayer getVideoPlaybackState]) {
        case MPMoviePlaybackStatePlaying:
            [self.playVideoButton setHidden:YES];
            [self.playVideoButton.superview sendSubviewToBack:self.playVideoButton];
            break;
        case MPMoviePlaybackStatePaused:
        case MPMoviePlaybackStateStopped:
            [self.playVideoButton setHidden:NO];
            [self.playVideoButton.superview bringSubviewToFront:self.playVideoButton];
            break;
        default:
            break;
    }
}

- (void)moviePlaybackWillEnterFullscreen:(NSNotification *)notification
{
    self.isVideoFullscreen = YES;
}

- (void)moviePlaybackDidExitFullscreen:(NSNotification *)notification
{
    self.isVideoFullscreen = NO;
}

#pragma mark - Outlet actions

- (IBAction)playVideoButtonClicked:(UIButton *)sender
{
    if([self.videoPlayer.view isHidden]) {
        [self.videoThumbnail setHidden:YES];
        [self.videoPlayer.view setHidden:NO];
    }
    if([self.videoPlayer getVideoPlaybackState] == MPMoviePlaybackStatePaused ||
       [self.videoPlayer getVideoPlaybackState] == MPMoviePlaybackStateStopped) {
        [self.videoPlayer startPlayer];
    } else if([self.videoPlayer getVideoPlaybackState] == MPMoviePlaybackStatePlaying) {
        [self.videoPlayer pausePlayer];
    }
}

- (void)toggleDidFinish:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        self.alertVC.view.alpha = 0;
    } completion:^(BOOL finished) {
        [self.alertVC.view removeFromSuperview];
        self.alertVC = nil;
    }];
    
    self.view.userInteractionEnabled=YES;
    int popupbuttonTag = [notification.object intValue];
    
    if (popupbuttonTag == 1) {
        self.startButton.hidden = YES;
        self.progressBar.hidden = YES;
        self.cancelButtonView.hidden = NO;
        
        [self disableTimerView];
        [self disableDelayView];
        [self disableRepeatView];
        [self beforeStartTimer];
    }
}

- (void)toggleTest
{
    self.view.userInteractionEnabled=NO;
    self.TestToggleVC = [[ToggleViewControllerTest alloc] initWithNibName:@"ToggleViewControllerTest" bundle:[NSBundle mainBundle]];
    [self.TestToggleVC.view.subviews[0] setAlpha:0.7];
    self.TestToggleVC.view.alpha = 0;
    self.TestToggleVC.bg.image = [self.TestToggleVC.bg.image resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)];
    [APP_DELEGATE.window addSubview:self.TestToggleVC.view];
    NSString *approved = [self.exercise stringForColumn:NSLocalizedString(@"approvedText", nil)];
    NSString *provFailed = [self.exercise stringForColumn:NSLocalizedString(@"failedText", nil)];

    NSLog(@"approved = %@  and failed = %@",approved,provFailed);
    self.TestToggleVC.approvedTextView.text = [self.exercise stringForColumn:NSLocalizedString(@"approvedText", nil)];
    self.TestToggleVC.failedTextView.text = [self.exercise stringForColumn:NSLocalizedString(@"failedText", nil)];
    [self addChildViewController:self.TestToggleVC];
    [UIView animateWithDuration:0.3 animations:^{
        self.TestToggleVC.view.alpha = 1;
    }];
    
    self.TestToggleVC.delegate = self;
    
}

- (void)didFinishTestToggle:(BOOL)answer
{
    [UIView animateWithDuration:0.3 animations:^{
        self.TestToggleVC.view.alpha = 0;
        
    } completion:^(BOOL finished) {
        [self.TestToggleVC.view removeFromSuperview];
        self.TestToggleVC = nil;
    }];
    
    self.view.userInteractionEnabled=NO;
    self.testToggleDone = [[ToggleViewControllerDone alloc] initWithNibName:@"ToggleViewControllerDone" bundle:[NSBundle mainBundle]];
    // [self.testToggleDone.view.subviews[0] setAlpha:0.7];
    self.testToggleDone.infoLabel.text = NSLocalizedString(@"level_completed", nil);
    self.testToggleDone.view.alpha = 0;
    self.testToggleDone.bg.image = [self.TestToggleVC.bg.image resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)];
    [APP_DELEGATE.window addSubview:self.testToggleDone.view];
    
    self.view.userInteractionEnabled=YES;
    if (answer){
        [self.exerciseDB finishExerciseStepWithId:self.exerciseId];
        self.exercises = [self.exerciseDB selectTestExerciseForLevel:self.levelId];
        self.exercise = [self.exercises rowAtIndex:0];
        self.progressBar.progress = 0;
        [self.levelDB completeLevelWithId:self.levelId];
        [self.levelDB makeAccessibleLevelWithId:[NSNumber numberWithInteger:([self.levelId integerValue]+1)]];
        
        NSString *text = NSLocalizedString(@"level_completed", nil);
        if ([self.levelId intValue] == 7) {
               text = NSLocalizedString(@"all_levels_completed", nil);
        }
        
        self.testToggleDone.infoLabel.text = text;
    } else {
        [self.exerciseDB cancelExerciseWithId:self.exerciseId];
        self.exercises = [self.exerciseDB selectTestExerciseForLevel:self.levelId];
        self.exercise = [self.exercises rowAtIndex:0];
        self.progressBar.progress = 0;
        self.testToggleDone.infoLabel.text = NSLocalizedString(@"level_failed", nil);
        
        [self cancelButtonPressed : nil];
        
    }
    
    [self addChildViewController:self.testToggleDone];
    [UIView animateWithDuration:0.3 animations:^{
        self.testToggleDone.view.alpha = 1;
    }];
}

- (IBAction)startButtonClicked:(UIButton *)sender
{
    self.startButton.hidden = YES;
    self.progressBar.hidden = YES;
    self.cancelButtonView.hidden = NO;
    
    [self disableTimerView];
    [self disableDelayView];
    [self disableRepeatView];
    [self playBeepSound];
    [self performSelector:@selector(beforeStartTimer) withObject:nil afterDelay:0.3];
    [self performSelector:@selector(playEndSound) withObject:nil afterDelay:3.0];
    
    
    
    NSInteger stepsToRepeat = [self.exercise intForColumn:@"required_steps"] - [self.exercise intForColumn:@"completed_steps"];
    self.repeatNumberLabel.text = [NSString stringWithFormat:@"%d %@", stepsToRepeat, NSLocalizedString(@"times", nil)];
    self.repeatNumberXLabel.text = [NSString stringWithFormat:@"%dx", stepsToRepeat];
}

- (void)beforeStartTimer
{
    startDate = [NSDate date];
    timerSeconds = 3;
    self.cancelButtonCountdownLabel.text = [NSString stringWithFormat:@"%d", timerSeconds];
    
    globalTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                   target:self
                                                 selector:@selector(startButtonTimerIteration:)
                                                 userInfo:nil
                                                  repeats:YES];
}

- (IBAction)cancelButtonPressed:(UIButton *)sender
{
    [globalTimer invalidate];
    self.cancelButtonView.hidden = YES;
    self.progressBar.hidden = YES;
    self.progressBar.progress = 0;
    self.startButton.hidden = NO;
    
    self.timerSecondsLabel.text = [NSString stringWithFormat:@"%d %@", [self.exercise intForColumn:@"length"], NSLocalizedString(@"second", nil)];
    self.delaySecondsLabel.text = [NSString stringWithFormat:@"%d %@", [self.exercise intForColumn:@"delay"], NSLocalizedString(@"second", nil)];
    NSInteger stepsToRepeat = [self.exercise intForColumn:@"required_steps"] - [self.exercise intForColumn:@"completed_steps"];
    self.repeatNumberLabel.text = [NSString stringWithFormat:@"%d %@", stepsToRepeat, NSLocalizedString(@"times", nil)];
    self.repeatNumberXLabel.text = [NSString stringWithFormat:@"%dx", stepsToRepeat];
    
    [self enableTimerView];
    [self enableDelayView];
    [self enableRepeatView];
}

- (IBAction)backButtonPressed:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)pauseButtonPressed:(UIButton *)sender
{
    [timerStorage addObject:[NSDate date]];
    
    if ([globalTimer isValid]) {
        [globalTimer invalidate];
        self.pauseButtonTextLabel.text = NSLocalizedString(@"timer_continue", nil);
        [UIApplication sharedApplication].idleTimerDisabled = NO;
    } else {
        self.pauseButtonTextLabel.text = NSLocalizedString(@"timer_pause", nil);
        [UIApplication sharedApplication].idleTimerDisabled = YES;
        if(currentState == TIMER_STATE_EXERCISE) {
            globalTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                           target:self
                                                         selector:@selector(exerciseTimerIteration:)
                                                         userInfo:nil
                                                          repeats:YES];
        } else if(currentState == TIMER_STATE_PAUSE) {
            globalTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                           target:self
                                                         selector:@selector(delayTimerIteration:)
                                                         userInfo:nil
                                                          repeats:YES];
        }
    }
}

#pragma mark - Timer functions

- (void)startButtonTimerIteration:(NSTimer *)timer
{
    NSDate *currentDate = [NSDate date];
    NSTimeInterval elapsedTime = [currentDate timeIntervalSinceDate:startDate];
    NSTimeInterval difference = round(timerSeconds - elapsedTime);
    
    NSLog(@"difference %f",difference);
    
    if (difference <= 0) {
        [timer invalidate];
        [self startExercise];
        if (!self.beepPlayer.playing) {
            [self performSelector:@selector(playEndSound) withObject:nil afterDelay:0.3];
        }
    } else {
        self.cancelButtonCountdownLabel.text = [NSString stringWithFormat:@"%.0f", difference];
    }
}

- (void)exerciseTimerIteration:(NSTimer *)timer
{
    NSDate *currentDate = [NSDate date];
    NSTimeInterval elapsedTime = 0;
    
    for (int i = 0; i < timerStorage.count - 1; i += 2) {
        if (i <= timerStorage.count - 1) {
            elapsedTime += [[timerStorage objectAtIndex:i+1] timeIntervalSinceDate:[timerStorage objectAtIndex:i]];
        }
    }
    if (timerStorage.count) {
        elapsedTime += [currentDate timeIntervalSinceDate:[timerStorage objectAtIndex:timerStorage.count-1]];
    }
    
    NSTimeInterval difference = timerSeconds - elapsedTime;
    
    // NSLog(@"exerciseTimerIteration %f",difference);
    
    if(round(difference) == 3) {
        if (!self.beepPlayer.playing) {
            [self playBeepSound];
        }
    }
    
    if(difference <= 0) {
        [timer invalidate];
        [self finishExercise];
        if (!self.beepEndingPlayer.playing) {
            [self performSelector:@selector(playEndSound) withObject:nil afterDelay:0.0];
        }
        return;
    }
    
    self.progressBar.progress = 100 * elapsedTime / timerSeconds;
    self.timerSecondsLabel.text = [NSString stringWithFormat:@"%.0f %@", difference, NSLocalizedString(@"second", nil)];
}

- (void)delayTimerIteration:(NSTimer *)timer
{
    NSDate *currentDate = [NSDate date];
    NSTimeInterval elapsedTime = 0;
    
    for (int i = 0; i < timerStorage.count - 1; i += 2) {
        if (i <= timerStorage.count - 1) {
            elapsedTime += [[timerStorage objectAtIndex:i+1] timeIntervalSinceDate:[timerStorage objectAtIndex:i]];
        }
    }
    if (timerStorage.count) {
        elapsedTime += [currentDate timeIntervalSinceDate:[timerStorage objectAtIndex:timerStorage.count-1]];
    }
    
    NSTimeInterval difference = timerSeconds - elapsedTime;
    
    if(round(difference) == 3) {
        if (!self.beepPlayer.playing)
        {
            [self playBeepSound];
        }
    }
    
    if(difference <= 0) {
        [timer invalidate];
        [self finishDelay];
        
        if (!self.beepEndingPlayer.playing) {
            [self performSelector:@selector(playEndSound) withObject:nil afterDelay:0.0];
            
        }
        return;
    }
    
    self.progressBar.progress = 100 * elapsedTime / timerSeconds;
    self.delaySecondsLabel.text = [NSString stringWithFormat:@"%.0f %@", difference, NSLocalizedString(@"second", nil)];
}


- (void)playBeepSound
{
    [self.beepPlayer play];
    NSLog(@"Beep Play");
}

- (void)playBeginSound
{
    [self.beepBeginingPlayer play];
    NSLog(@"Beep Begin Play");
    
}

- (void)playEndSound
{
    [self.beepEndingPlayer play];
    NSLog(@"Beep End Play");
}

#pragma mark - helper functions

- (void)startExercise
{
    currentState = TIMER_STATE_EXERCISE;
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    NSInteger timeBetweenExercies = 20 * 60;
    
    if ([self.exercise intForColumn:@"completed_steps"] == 0) {
        NSInteger lastTimestamp = [self.levelDB getLastStepTimeForLevelId:self.levelId];
        NSInteger now = [[NSDate date] timeIntervalSince1970];
        if (now - lastTimestamp >= timeBetweenExercies && [self.levelDB getRemainingStepsCountForLevelId:self.levelId] > 0) {
            [self.levelDB completeLevelStepWithId:self.levelId];
        }
    }
    
    [self enableTimerView];
    self.cancelButtonView.hidden = YES;
    self.progressBar.hidden = NO;
    self.progressBar.labelText = NSLocalizedString(@"exercise", nil);
    
    [timerStorage removeAllObjects];
    [timerStorage addObject:[NSDate date]];
    timerSeconds = [self.exercise intForColumn:@"length"];
    [globalTimer invalidate];
    globalTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                   target:self
                                                 selector:@selector(exerciseTimerIteration:)
                                                 userInfo:nil
                                                  repeats:YES];
}

- (void)startDelay
{
    currentState = TIMER_STATE_PAUSE;
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    [self enableDelayView];
    self.progressBar.labelText = NSLocalizedString(@"rest", nil);
    
    [timerStorage removeAllObjects];
    [timerStorage addObject:[NSDate date]];
    timerSeconds = [self.exercise intForColumn:@"delay"];
    globalTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                   target:self
                                                 selector:@selector(delayTimerIteration:)
                                                 userInfo:nil repeats:YES];
}

- (void)finishExercise
{
    currentState = TIMER_STATE_UNKNOWN;
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    
    [self disableTimerView];
    
    NSInteger stepsToGo = [self.exercise intForColumn:@"required_steps"] - [self.exercise intForColumn:@"completed_steps"];
    self.timerSecondsLabel.text = [NSString stringWithFormat:@"%d %@", [self.exercise intForColumn:@"length"], NSLocalizedString(@"second", nil)];
    
    if (stepsToGo > 0) {
        [self.exerciseDB finishExerciseStepWithId:self.exerciseId];
    }
    self.exercises = [self.exerciseDB selectExercises:self.exerciseId];
    self.exercise = [self.exercises rowAtIndex:0];
    
    self.progressBar.progress = 0;
    
    stepsToGo = [self.exercise intForColumn:@"required_steps"] - [self.exercise intForColumn:@"completed_steps"];
    self.repeatNumberLabel.text = [NSString stringWithFormat:@"%d %@", stepsToGo, NSLocalizedString(@"times", nil)];
    self.repeatNumberXLabel.text = [NSString stringWithFormat:@"%dx", stepsToGo];
 
    if (stepsToGo == 0) {
        [self.exerciseDB completeExerciseWithId:self.exerciseId];
        
        [self scheduleLocalNotification];
    }
    
    [self startDelay];
}

- (void)scheduleLocalNotification
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];

    UILocalNotification *notification = [[UILocalNotification alloc] init];

    NSTimeInterval interval = 3 * 24 * 60 * 60;
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:interval];
    notification.fireDate = date;
//    notification.repeatInterval = NSMinuteCalendarUnit;
    notification.alertBody = NSLocalizedString(@"notification_text", nil);
    notification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:notification ];

}

- (void)checkForCompletedWorkoutsAndShowMotivatingAlert
{
    NSNumber *totalCompletedExercisesInLevel = [[DBLevel sharedInstance] getTotalCompletedExercisesCountForLevelId:self.levelId];
    if ([totalCompletedExercisesInLevel intValue] == 9)
    {
        NSString *motivated_text = [[DBLevel sharedInstance] getMotivatingTextForLevelId:self.levelId];
        if (![motivated_text length])
            return;
        
        self.view.userInteractionEnabled=NO;
        self.exerciseToggleDone = [[ToggleViewControllerDone alloc] initWithNibName:@"ToggleViewControllerDone" bundle:[NSBundle mainBundle]];
        // [self.testToggleDone.view.subviews[0] setAlpha:0.7];
        self.exerciseToggleDone.view.alpha = 0;
        self.exerciseToggleDone.bg.image = [[UIImage imageNamed:@"popup_bg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)];
        [APP_DELEGATE.window addSubview:self.exerciseToggleDone.view];
        
        self.view.userInteractionEnabled=YES;
        
        self.exerciseToggleDone.infoLabel.text = motivated_text;
        
        [self addChildViewController:self.exerciseToggleDone];
        [UIView animateWithDuration:0.3 animations:^{
            self.exerciseToggleDone.view.alpha = 1;
        }];
    }
}

- (void)finishDelay
{
    currentState = TIMER_STATE_UNKNOWN;
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    
    [self disableDelayView];
    NSInteger stepsToGo = [self.exercise intForColumn:@"required_steps"] - [self.exercise intForColumn:@"completed_steps"];
    
    if(stepsToGo == 0) {
        [self enableTimerView];
        [self enableDelayView];
        [self enableRepeatView];
        
        self.progressBar.hidden = YES;
        self.startButton.hidden = NO;
        
        if(self.isTest) {
            [self toggleTest];
        }
        
        self.exercises = [self.exerciseDB selectExercises:self.exerciseId];
        self.exercise = [self.exercises rowAtIndex:0];
        
        [self checkForCompletedWorkoutsAndShowMotivatingAlert];
    } else {
        [self startExercise];
    }
    self.progressBar.progress = 0;
    
    self.delaySecondsLabel.text = [NSString stringWithFormat:@"%d %@", [self.exercise intForColumn:@"delay"], NSLocalizedString(@"second", nil)];
}

- (void)disableTimerView
{
    self.timerIcon.hidden = YES;
    self.timerIconInactive.hidden = NO;
    for(UIView *subview in self.timerDetailsView.subviews) {
        if([subview isKindOfClass:[UILabel class]]) {
            ((UILabel *)subview).textColor = [Utils colorWithHex:@"#8B8B8B"];
        }
    }
}

- (void)enableTimerView
{
    self.timerIcon.hidden = NO;
    self.timerIconInactive.hidden = YES;
    for(UIView *subview in self.timerDetailsView.subviews) {
        if([subview isKindOfClass:[UILabel class]]) {
            ((UILabel *)subview).textColor = [Utils colorWithHex:TEXT_COLOR];
        }
    }
}

- (void)disableDelayView
{
    self.delayIcon.hidden = YES;
    self.delayIconInactive.hidden = NO;
    for(UIView *subview in self.delayDetailsView.subviews) {
        if([subview isKindOfClass:[UILabel class]]) {
            ((UILabel *)subview).textColor = [Utils colorWithHex:@"#8B8B8B"];
        }
    }
}

- (void)enableDelayView
{
    self.delayIcon.hidden = NO;
    self.delayIconInactive.hidden = YES;
    for(UIView *subview in self.delayDetailsView.subviews) {
        if([subview isKindOfClass:[UILabel class]]) {
            ((UILabel *)subview).textColor = [Utils colorWithHex:TEXT_COLOR];
        }
    }
}

- (void)disableRepeatView
{
    self.repeatIcon.hidden = YES;
    self.repeatIconInactive.hidden = NO;
    for(UIView *subview in self.repeatDetailsView.subviews) {
        if([subview isKindOfClass:[UILabel class]]) {
            ((UILabel *)subview).textColor = [Utils colorWithHex:@"#8B8B8B"];
        }
    }
}

- (void)enableRepeatView
{
    self.repeatIcon.hidden = NO;
    self.repeatIconInactive.hidden = YES;
    for(UIView *subview in self.repeatDetailsView.subviews) {
        if([subview isKindOfClass:[UILabel class]]) {
            ((UILabel *)subview).textColor = [Utils colorWithHex:TEXT_COLOR];
        }
    }
}

@end
