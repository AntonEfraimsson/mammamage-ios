//
//  ExerciseCell.h
//  MammaMage
//
//  Created by Narek Haytyan on 10/27/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExerciseCell : UITableViewCell

@property (nonatomic) NSInteger exerciseNumber;
@property (strong, nonatomic) NSString *exerciseName;

@end
