//
//  ExerciseCell.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/27/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "ExerciseCell.h"

static UIFont *cellFont;

@interface ExerciseCell ()

@property (weak, nonatomic) IBOutlet UILabel *exerciseNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *exerciseNumberImageView;

@end


@implementation ExerciseCell


#pragma mark - Custom getters/setters

- (void)setExerciseNumber:(NSInteger)exerciseNumber
{
    _exerciseNumber = exerciseNumber;
    self.exerciseNumberImageView.image = [UIImage imageNamed:[@"symbol_"
                                                              stringByAppendingString:[NSString stringWithFormat:@"%d.png", exerciseNumber]]];
}

- (void)setExerciseName:(NSString *)exerciseName
{
    _exerciseName = exerciseName;
    self.exerciseNameLabel.text = exerciseName;
}

- (void)setFrame:(CGRect)frame
{
    if (IS_IOS_7) {
        frame.origin.x += 10;
        frame.size.width -= 2 * 10;
    }

    [super setFrame:frame];
}

#pragma mark - View lifecycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if (!cellFont) {
        cellFont = [UIFont fontWithName:FONT_NORMAL
                                   size:self.exerciseNameLabel.font.pointSize];
    }
    self.exerciseNameLabel.font = cellFont;
}

@end
