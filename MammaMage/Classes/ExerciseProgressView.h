//
//  ExerciseProgressView.h
//  MammaMage
//
//  Created by Narek Haytyan on 11/04/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExerciseProgressView : UIView

@property (nonatomic) NSInteger levelNumber;
@property (nonatomic) double progress;
@property (strong, nonatomic) NSString *labelText;

@end
