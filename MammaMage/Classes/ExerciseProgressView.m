//
//  ExerciseProgressView.m
//  MammaMage
//
//  Created by Narek Haytyan on 11/04/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "ExerciseProgressView.h"

@interface ExerciseProgressView ()

@property (strong, nonatomic) IBOutlet UIView *progressIndicatorView;
@property (strong, nonatomic) IBOutlet UILabel *label;

@end

@implementation ExerciseProgressView

- (void)awakeFromNib
{
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"exercise_progressbar_none.png"]];
    self.label.font = [UIFont fontWithName:FONT_BOLD
                                      size:self.label.font.pointSize];
}

- (void) setLevelNumber:(NSInteger)levelNumber
{
    _levelNumber = levelNumber;
    
    self.progressIndicatorView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[@"exercise_progressbar_level"
                                                                                                     stringByAppendingString:[NSString stringWithFormat:@"%d.png", levelNumber]]]];
}

- (void)setProgress:(double)progress
{
    progress = fabs(progress);
    if(progress > 100) {
        progress = 100;
    }
    
    CGRect frame = self.progressIndicatorView.frame;
    frame.size.width = self.frame.size.width * progress / 100;
    self.progressIndicatorView.frame = frame;
}

- (void)setLabelText:(NSString *)labelText
{
    _labelText = labelText;
    self.label.text = labelText;
}

@end
