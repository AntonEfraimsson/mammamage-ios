//
//  EverydayVC.h
//  MammaMage
//
//  Created by Narek Haytyan on 10/24/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "VideoPlayerViewController.h"
#import <UIKit/UIKit.h>

@class VideoPlayerViewController;
@interface IVardagenVC : UIViewController <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *bufferImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *bufferImageView2;
@property (strong, nonatomic) VideoPlayerViewController *videoPlayer1;
@property (strong, nonatomic) VideoPlayerViewController *videoPlayer2;

@end
