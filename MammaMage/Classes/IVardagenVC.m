//
//  EverydayVC.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/24/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//


// #import "ChuckNorris.h"
#import "IVardagenVC.h"
#import "CustomMoviePlayerVC.h"
#import "Constants.h"
#import "VideoPlayerViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "DDPageControl.h"


@interface IVardagenVC () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet DDPageControl *pageControll1;
@property (weak, nonatomic) IBOutlet DDPageControl *pageControll2;

@property (weak, nonatomic) IBOutlet UIScrollView *photo1;
@property (weak, nonatomic) IBOutlet UIScrollView *photo2;

@property (strong, nonatomic) UIImageView *currentBufferIV;

@property (strong, nonatomic) VideoPlayerViewController *currentPlayer;

@property (weak, nonatomic) IBOutlet UIView *videoBuffer1;
@property (weak, nonatomic) IBOutlet UIView *videoBuffer2;
@property (strong, nonatomic) UIView *currentVideoBuffer;

@property (weak, nonatomic) IBOutlet UIButton *playVideoButton;
@property (weak, nonatomic) IBOutlet UIButton *playVideoButton2;

@property (strong, nonatomic) UIButton *currentPlayVideoButton;

@property (nonatomic) BOOL isVideoFullscreen;

@property (weak, nonatomic) IBOutlet UIWebView *webView;


@property (weak, nonatomic) IBOutlet UIScrollView *contentScroll;

- (IBAction)playButtonPressed:(UIButton *)sender;
- (IBAction)changePage:(DDPageControl *)sender;

@end

@implementation IVardagenVC


- (int)screenResulution
{
    return [UIScreen mainScreen].bounds.size.width;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // creating new back button with an empty title
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc] initWithTitle:@" "
                                                                style:UIBarButtonItemStylePlain
                                                               target:nil
                                                               action:nil];
    self.navigationItem.backBarButtonItem = backBtn;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.jpg"]];
    
    // unhiding navigation bar
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
    
    //Registering Observers
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(playerItemDidReachEnd:)
//                                                 name:AVPlayerItemDidPlayToEndTimeNotification
//                                               object:nil];
//    
//    VideoPlayerViewController *player = [[VideoPlayerViewController alloc] init];
//    player.URL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Film_14" ofType:@"mp4"]];
//    player.view.frame = CGRectMake(0, 0, self.videoBuffer1.frame.size.width, self.videoBuffer1.frame.size.height);
//    [self.videoBuffer1 addSubview:player.view];
//    self.videoPlayer1 = player;
//    self.videoPlayer1.view.hidden = YES;
//    self.videoPlayer1.view.tag  = 1;
//    self.videoBuffer1.hidden = YES;
//    self.playVideoButton.hidden = NO;
//    self.videoPlayer1.delegate = self;
//    
//    VideoPlayerViewController *player2 = [[VideoPlayerViewController alloc] init];
//    player2.URL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Film_15" ofType:@"mp4"]];
//    player2.view.frame = CGRectMake(0, 0, self.videoBuffer2.frame.size.width, self.videoBuffer2.frame.size.height);
//    [self.videoBuffer2 addSubview:player2.view];
//    self.videoPlayer2 = player2;
//    self.videoPlayer2.view.hidden = YES;
//    self.videoPlayer2.view.tag  = 2;
//    self.videoBuffer2.hidden = YES;
//    self.playVideoButton2.hidden = NO;
//    self.videoPlayer2.delegate = self;
//    
//    self.contentScroll.contentSize = CGSizeMake(self.contentScroll.frame.size.width, 3560);
//    
//    UITapGestureRecognizer *tap1;
//    tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bufferTaped1:)];
//    
//    [self.videoPlayer1.view addGestureRecognizer:tap1];
//    
//    UITapGestureRecognizer *tap2;
//    tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bufferTaped2:)];
//    
//    [self.videoPlayer2.view addGestureRecognizer:tap2];
//    
//    //DDPage Control
//    [self.pageControll1 setNumberOfPages:3];
//    [self.pageControll1 setIndicatorDiameter:7];
//    [self.pageControll1 setOnColor:[UIColor whiteColor]];
//    [self.pageControll1 setOffColor:[UIColor darkGrayColor]];
//    
//    [self.pageControll2 setNumberOfPages:3];
//    [self.pageControll2 setIndicatorDiameter:7];
//    [self.pageControll2 setOnColor:[UIColor whiteColor]];
//    [self.pageControll2 setOffColor:[UIColor darkGrayColor]];
    
    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"IVardagen.html" withExtension:nil];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:URL];
    [self.webView loadRequest:requestObj];
    self.webView.backgroundColor = [UIColor clearColor];
    self.webView.opaque = NO;
    self.webView.scrollView.bounces = NO;
    self.webView.delegate = self;
    
    //Set localized title
    [self.navigationItem setTitle: NSLocalizedString(@"menu_everyday_button", nil)];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([request.URL.absoluteString hasPrefix:@"http"]) {
        [[UIApplication sharedApplication] openURL:request.URL];
        return NO;
    }
    return YES;
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
    [self bufferTaped1:nil];
    [self bufferTaped2:nil];
}

- (void)bufferTaped1: (UITapGestureRecognizer*) tapObj {
    
    [self.videoPlayer1 pause];
    self.videoPlayer1.view.hidden = YES;
    self.videoBuffer1.hidden = YES;
    self.playVideoButton.hidden = NO;
    self.bufferImageView1.hidden = NO;
    
    self.bufferImageView1.image = [self.videoPlayer1 screenshotFromPlayer:self.videoPlayer1.view.frame.size];
}


- (void)bufferTaped2: (UITapGestureRecognizer*) tapObj {
    
    [self.videoPlayer2 pause];
    self.videoBuffer2.hidden = YES;
    self.videoPlayer2.view.hidden = YES;
    self.playVideoButton2.hidden = NO;
    self.bufferImageView2.hidden = NO;
    
    self.bufferImageView2.image = [self.videoPlayer2 screenshotFromPlayer:self.videoPlayer2.view.frame.size];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //scroll1
    CGRect frame  = self.photo1.frame;
    frame.size.height = IMAGE_HEIGHT;
    self.photo1.frame = frame;
    self.photo1.contentSize = self.photo1.frame.size;
    frame.size.width = 320;
    self.photo1.frame = frame;
    
    //scroll2
    frame  = self.photo2.frame;
    frame.size.height = IMAGE_HEIGHT;
    self.photo2.frame = frame;
    self.photo2.contentSize = self.photo2.frame.size;
    frame.size.width = 320;
    self.photo2.frame = frame;
    
    
}

- (IBAction)playButtonPressed:(UIButton *)sender {
    
    if (sender.tag == 1) {
        
        [self.currentPlayer pause];
        [self.videoPlayer1 play];
        
        self.bufferImageView1.hidden = YES;
        //sender.hidden = YES;
        
        self.videoPlayer1.view.Hidden = NO;
        self.videoBuffer1.hidden = NO;
        
        self.currentPlayer = self.videoPlayer1;
        self.currentBufferIV = self.bufferImageView1;
        self.currentVideoBuffer = self.videoBuffer1;
        self.currentPlayVideoButton = self.playVideoButton;
    }
    else if (sender.tag == 2) {
        
        [self.currentPlayer pause];
        [self.videoPlayer2 play];
        self.bufferImageView2.hidden = YES;
        //sender.hidden = YES;
        
        self.videoPlayer2.view.Hidden = NO;
        self.videoBuffer2.hidden = NO;
        
        self.currentPlayer = self.videoPlayer2;
        self.currentBufferIV = self.bufferImageView2;
        self.currentVideoBuffer = self.videoBuffer2;
        self.currentPlayVideoButton = self.playVideoButton2;
        
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.currentPlayer stop];
    self.currentPlayer = nil;
    self.contentScroll.delegate = nil;
    self.photo1.delegate = nil;
    self.photo2.delegate = nil;
}


//Photo Scroll View Delegates
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    //    NSLog(@"%i", scrollView.tag);
    int resulution;
    int page;
    
    if (scrollView.tag == 1) {
        resulution = [UIScreen mainScreen].bounds.size.width;
        page = self.photo1.contentOffset.x/resulution;
        [self.pageControll1 setCurrentPage:page];
    }
    else if (scrollView.tag == 2) {
        resulution = [UIScreen mainScreen].bounds.size.width;
        page = self.photo2.contentOffset.x/resulution ;
        [self.pageControll2 setCurrentPage:page];
    }
}


// Page Controll
- (IBAction)changePage:(DDPageControl *)sender {
    
    
    int resulution = [UIScreen mainScreen].bounds.size.width;
    int page =  sender.currentPage;
    
    if (sender.tag == 1) {
        CGPoint offset = self.photo1.contentOffset;
        
        offset.x = page * resulution;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.photo1.contentOffset = offset;}];
    }
    
    else if (sender.tag == 2) {
        CGPoint offset = self.photo2.contentOffset;
        
        offset.x = page * resulution;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.photo2.contentOffset = offset;}];
    }
}

@end
