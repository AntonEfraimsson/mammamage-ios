//
//  LevelCell.h
//  MammaMage
//
//  Created by Narek Haytyan on 11/01/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LevelCell : UITableViewCell

@property (strong, nonatomic) NSString* levelDescription;
@property (strong, nonatomic) NSString* levelCaption;
@property (strong, nonatomic) UIImage* levelNumber;
@property (strong, nonatomic) UIImage* arrowImage;
@property (nonatomic) BOOL isLast;

- (void)updateStarProgress:(double)percent
                  forLevel:(NSNumber *)levelId;
- (void)levelCaptionWithColor:(UIColor *)color;
- (void)disableCellWithIndex:(NSInteger)index;
- (void)enableCellWithIndex:(NSInteger)index;

@end
