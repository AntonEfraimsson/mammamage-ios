//
//  LevelCell.m
//  MammaMage
//
//  Created by Narek Haytyan on 11/01/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "LevelCell.h"

static UIFont *captionFont;
static UIFont *descriptionFont;

@interface LevelCell()

@property (weak, nonatomic) IBOutlet UILabel *levelCaptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UIImageView *levelNumberView;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

@property (weak, nonatomic) IBOutlet UIView *starContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *grayStarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *coloredStarImageView;


// colors
@property (strong, nonatomic) NSArray *levelColors;
@property (strong, nonatomic) UIColor *descriptionColor;
@property (strong, nonatomic) UIColor *inactiveColor;

@end

@implementation LevelCell

- (void)awakeFromNib
{
    if(!captionFont) {
        captionFont = [UIFont fontWithName:FONT_BOLD size:self.levelCaptionLabel.font.pointSize];
    }
    if(!descriptionFont) {
        descriptionFont = [UIFont fontWithName:FONT_NORMAL size:self.levelDescriptionLabel.font.pointSize];
    }
    
    // initializing cell fonts
    self.levelCaptionLabel.font = captionFont;
    self.levelDescriptionLabel.font = descriptionFont;
    
    // initializing level colors
    self.levelColors = @[[Utils colorWithHex:@"#e56db7"],    // color for level 1
                         [Utils colorWithHex:@"#46aedf"],    // color for level 2
                         [Utils colorWithHex:@"#29cf87"],    // color for level 3
                         [Utils colorWithHex:@"#abd925"],    // color for level 4
                         [Utils colorWithHex:@"#e72d63"],    // color for level 5
                         [Utils colorWithHex:@"#bb32ee"],    // color for level 6
                         [Utils colorWithHex:@"#3557e6"]];   // color for level 7
    self.descriptionColor = [Utils colorWithHex:TEXT_COLOR]; // color for description
    self.inactiveColor = [Utils colorWithHex:@"#cfcfcf"];    // color for inactive text
}

- (void)levelCaptionWithColor:(UIColor *)color
{
    self.levelCaptionLabel.textColor = color;
    self.levelCaptionLabel.highlightedTextColor = color;
}

- (void)disableCellWithIndex:(NSInteger)index
{
    [self levelCaptionWithColor:self.inactiveColor];
    if (self.isLast) {
        self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_level_last_bg_inactive.png"]];
    } else {
        self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_level_bg_inactive.png"]];
    }
    self.backgroundView.contentMode = UIViewContentModeCenter;
    self.levelDescriptionLabel.textColor = self.inactiveColor;
    self.arrowImage = [UIImage imageNamed:@"arrow_inactive.png"];
    self.levelNumber = [UIImage imageNamed:[@"nr_level" stringByAppendingString:[NSString stringWithFormat:@"%d_inactive.png", index+1]]];
    self.userInteractionEnabled = NO;
}

- (void)enableCellWithIndex:(NSInteger)index
{
    [self levelCaptionWithColor:[self.levelColors objectAtIndex:index]];
    if (self.isLast) {
        self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_level_last_bg_normal.png"]];
    } else {
        self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_level_bg_normal.png"]];
    }
    self.backgroundView.contentMode = UIViewContentModeCenter;
    
    if (self.isLast) {
        self.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_level_last_bg_highlight.png"]];
    } else {
        self.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_level_bg_highlight.png"]];
    }
    self.selectedBackgroundView.contentMode = UIViewContentModeCenter;
    self.levelDescriptionLabel.textColor = self.descriptionColor;
    self.arrowImage = [UIImage imageNamed:[@"arrow_level" stringByAppendingString:[NSString stringWithFormat:@"%d.png", index+1]]];
    self.levelNumber = [UIImage imageNamed:[@"nr_level" stringByAppendingString:[NSString stringWithFormat:@"%d.png", index+1]]];
    self.userInteractionEnabled = YES;
}

- (void)updateStarProgress:(double)percent
                  forLevel:(NSNumber *)levelId
{
    percent = fabs(percent);
    
    self.coloredStarImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"star_%d.png",[levelId intValue]]];
    
    CGFloat fullHeight = self.grayStarImageView.frame.size.height;
    
    CGRect frame = self.coloredStarImageView.frame;
    frame.size.height = fullHeight * percent/100.0f;
    frame.origin.y = fullHeight - frame.size.height;
    self.coloredStarImageView.frame = frame;
}

#pragma mark - custom getters/setters

- (void)setArrowImage:(UIImage *)arrowImage
{
    self.arrowImageView.image = arrowImage;
}

- (void)setLevelNumber:(UIImage *)levelNumberImage
{
    self.levelNumberView.image = levelNumberImage;
}

- (void)setLevelDescription:(NSString *)levelDescription
{
    self.levelDescriptionLabel.text = levelDescription;
}

- (void)setLevelCaption:(NSString *)levelCaption
{
    self.levelCaptionLabel.text = levelCaption;
}

@end
