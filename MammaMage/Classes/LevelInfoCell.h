//
//  LevelInfoCell.h
//  MammaMage
//
//  Created by Narek Haytyan on 10/29/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OHAttributedLabel.h"
#import "NSAttributedString+Attributes.h"

@interface LevelInfoCell : UITableViewCell

@property (strong, nonatomic) NSString* captionLabelText;
@property (strong, nonatomic) NSAttributedString* descriptionLabelText;
@property (strong, nonatomic) NSString* footerLabelText;
@property (strong, nonatomic) NSString* remainingStepsCountText;

- (void)updateProgressIndicator:(double)percent
                       forLevel:(NSNumber *)levelId;

- (void)updateStarProgress:(double)progress
                  forLevel:(NSNumber *)levelId;

@end
