//
//  LevelInfoCell.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/29/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "LevelInfoCell.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface LevelInfoCell()

@property (weak, nonatomic) IBOutlet UILabel *levelCaptionLabel;
@property (weak, nonatomic) IBOutlet OHAttributedLabel *levelDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelFooterLabel;
@property (weak, nonatomic) IBOutlet UIView *levelProgressView;
@property (weak, nonatomic) IBOutlet UIView *levelProgressImage;
@property (weak, nonatomic) IBOutlet UILabel *levelProgressCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelProgressTextLabel;
@property (weak, nonatomic) IBOutlet UIView *levelProgressTextView;
@property (weak, nonatomic) IBOutlet UIImageView *levelProgressTextArrow;
@property (weak, nonatomic) IBOutlet UIView *progressContainerView;

@property (weak, nonatomic) IBOutlet UIView *starContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *grayStarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *coloredStarImageView;

@property (weak, nonatomic) IBOutlet UIView *shareContainerView;
@property (weak, nonatomic) IBOutlet UILabel *completedTitleLabel;
@property (weak, nonatomic) IBOutlet OHAttributedLabel *completedDescriptionLabel;


@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageTop;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageMiddle;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageBottom;

@property (strong, nonatomic) NSNumber *level_id;
@end

@implementation LevelInfoCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.contentView.superview setClipsToBounds:NO];
    
    self.levelCaptionLabel.font = [UIFont fontWithName:FONT_BOLD
                                                  size:self.levelCaptionLabel.font.pointSize];
    self.levelDescriptionLabel.font = [UIFont fontWithName:FONT_NORMAL
                                                      size:self.levelDescriptionLabel.font.pointSize];
    self.levelFooterLabel.font = [UIFont fontWithName:FONT_BOLD
                                                 size:self.levelFooterLabel.font.pointSize];
    self.levelProgressCountLabel.font = [UIFont fontWithName:FONT_BOLD
                                                        size:self.levelProgressCountLabel.font.pointSize];
    self.levelProgressTextLabel.font = [UIFont fontWithName:FONT_NORMAL
                                                       size:self.levelProgressTextLabel.font.pointSize];
    
    self.completedTitleLabel.font = [UIFont fontWithName:FONT_BOLD
                                                    size:self.levelCaptionLabel.font.pointSize];
    self.completedDescriptionLabel.font = [UIFont fontWithName:FONT_NORMAL
                                                         size:self.levelDescriptionLabel.font.pointSize];
    
    self.levelProgressView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_none.png"]];
    
    self.completedTitleLabel.text = NSLocalizedString(@"level_completed_title", nil);
    self.completedDescriptionLabel.text = NSLocalizedString(@"level_completed_description", nil);
}

- (void)updateProgressIndicator:(double)percent
                       forLevel:(NSNumber *)levelId
{
    self.level_id = levelId;
    percent = fabs(percent);
    CGRect f;
    CGFloat offset = self.levelProgressView.frame.size.width * percent/100;
    
    self.levelProgressImage.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[@"bar_level" stringByAppendingString:[NSString stringWithFormat:@"%d.png", [levelId integerValue]]]]];
    
    self.levelProgressImage.frame = CGRectMake(0, 0, offset, self.levelProgressView.frame.size.height);
    
    f = self.levelProgressTextArrow.frame;
    self.levelProgressTextArrow.frame = CGRectMake(offset, f.origin.y, f.size.width, f.size.height);
    
    f = self.levelProgressTextView.frame;
    CGFloat leftOffset = offset + 10;
    CGFloat rightOffset = offset - 10;
    if (leftOffset > f.size.width/2 && rightOffset < self.levelProgressView.frame.size.width - f.size.width/2) {
        self.levelProgressTextView.frame = CGRectMake(leftOffset - f.size.width/2, f.origin.y, f.size.width, f.size.height);
    } else if (leftOffset <= f.size.width/2) {
        self.levelProgressTextView.frame = CGRectMake(0, f.origin.y, f.size.width, f.size.height);
    } else if (rightOffset >= self.levelProgressView.frame.size.width - f.size.width/2) {
        self.levelProgressTextView.frame = CGRectMake(self.levelProgressView.frame.size.width - f.size.width + 20, f.origin.y, f.size.width, f.size.height);
    }
}

- (void)updateStarProgress:(double)percent
                  forLevel:(NSNumber *)levelId
{
    percent = fabs(percent);
    
    self.coloredStarImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"star_large%d.png",[levelId intValue]]];
    
    CGFloat fullHeight = self.grayStarImageView.frame.size.height;
    
    CGRect frame = self.coloredStarImageView.frame;
    frame.size.height = fullHeight * percent/100.0f;
    frame.origin.y = fullHeight - frame.size.height;
    self.coloredStarImageView.frame = frame;
}

#pragma mark - custom getters/setters

- (void)setCaptionLabelText:(NSString *)captionLabelText
{
    _captionLabelText = captionLabelText;
    self.levelCaptionLabel.text = captionLabelText;
}

- (void)setFooterLabelText:(NSString *)footerLabelText
{
    _footerLabelText = footerLabelText;
    self.levelFooterLabel.text = footerLabelText;
}

- (void)setDescriptionLabelText:(NSAttributedString *)descriptionLabelText
{
    _descriptionLabelText = descriptionLabelText;
    
    self.levelDescriptionLabel.attributedText = descriptionLabelText;
    NSString *buffer = [descriptionLabelText string];
    CGRect frame = self.levelDescriptionLabel.frame;
    CGFloat oldHeight = frame.size.height;
    CGFloat newHeight = [buffer sizeWithFont:self.levelDescriptionLabel.font
                           constrainedToSize:CGSizeMake(frame.size.width, 999)
                               lineBreakMode:UILineBreakModeWordWrap].height;
    CGFloat heightDelta = newHeight - oldHeight ;
    frame.size.height = newHeight;
    self.levelDescriptionLabel.frame = frame;
    
    frame = self.levelFooterLabel.frame;
    frame.origin.y += heightDelta;
    self.levelFooterLabel.frame = frame;
    
    frame = self.progressContainerView.frame;
    frame.origin.y += heightDelta;
    self.progressContainerView.frame = frame;
    
    frame = self.starContainerView.frame;
    frame.origin.y += heightDelta;
    self.starContainerView.frame = frame;
    
    frame = self.backgroundImageMiddle.frame;
    frame.size.height += heightDelta;
    self.backgroundImageMiddle.frame = frame;
    
    frame = self.backgroundImageBottom.frame;
    frame.origin.y += heightDelta;
    self.backgroundImageBottom.frame = frame;
}

- (void)setRemainingStepsCountText:(NSString *)remainingStepsCountText
{
    _remainingStepsCountText = remainingStepsCountText;
    NSInteger count = [remainingStepsCountText integerValue];
    if(count > 0) {
        self.levelProgressTextArrow.hidden = NO;
        self.levelProgressTextView.hidden = NO;
        
        self.levelProgressCountLabel.text = remainingStepsCountText;
        self.levelProgressCountLabel.hidden = NO;
        if ([remainingStepsCountText intValue] == 1) {
            self.levelProgressTextLabel.text = NSLocalizedString(@"level_progress_text_singular", nil);
        } else {
            self.levelProgressTextLabel.text = NSLocalizedString(@"level_progress_text_plural", nil);
        }

        CGRect frame = self.levelProgressTextLabel.frame;
        frame.origin.x = 34;
        self.levelProgressTextLabel.frame = frame;
    }
//    else if (count == 1) {
//        self.levelProgressTextArrow.hidden = NO;
//        self.levelProgressTextView.hidden = NO;
//        
//        self.levelProgressCountLabel.hidden = YES;
//        self.levelProgressTextLabel.text = @"Genomför test för nästa nivå";
//        CGRect frame = self.levelProgressTextLabel.frame;
//        frame.origin.x = 12;
//        self.levelProgressTextLabel.frame = frame;
//    }
    else {
        self.levelProgressTextArrow.hidden = YES;
        self.levelProgressTextView.hidden = YES;
        
        //Add facebook Share and twitter share
        self.starContainerView.hidden = YES;
        self.shareContainerView.hidden = NO;
        [self.progressContainerView setY:[self.starContainerView y]];
        self.progressContainerView.height = self.progressContainerView.height + self.shareContainerView.height;
    }
}

- (void)setFrame:(CGRect)frame
{
    if (IS_IOS_7) {
        frame.origin.y -= 30;
        frame.origin.x += 10;
        frame.size.width -= 2 * 10;
        frame.size.height += 10;
    }
    [super setFrame:frame];
}

#pragma mark - Action
- (IBAction)twitterTapped:(id)sender
{
    NSLog(@"Twitter tapped");
    
    SLComposeViewController *twSheet = [SLComposeViewController
                                        composeViewControllerForServiceType:SLServiceTypeTwitter];

    [twSheet setInitialText:[NSString stringWithFormat:NSLocalizedString(@"sm_level_complete", nil),[self.level_id intValue]]];
    [twSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/se/app/mammamage/id593634082?mt=8"]];
    [APP_DELEGATE.window.rootViewController presentViewController:twSheet animated:YES completion:nil];
}

- (IBAction)facebookTapped:(id)sender
{
    NSLog(@"Facebook tapped");
    
    SLComposeViewController *fbSheet = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeFacebook];
    [fbSheet setInitialText:[NSString stringWithFormat:NSLocalizedString(@"sm_level_complete", nil),[self.level_id intValue]]];
    [fbSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/se/app/mammamage/id593634082?mt=8"]];
    [APP_DELEGATE.window.rootViewController presentViewController:fbSheet animated:YES completion:nil];
}



@end
