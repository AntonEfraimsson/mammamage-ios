//
//  MMNavigationBar.h
//  MammaMage
//
//  Created by Narek Haytyan on 10/31/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CWNavigationBar.h"

@interface MMNavigationBar : CWNavigationBar

@end
