//
//  MMNavigationBar.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/31/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "MMNavigationBar.h"

@implementation MMNavigationBar

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    for (UIView *view in self.subviews) {
        if ([[[view class] description] isEqualToString:@"UINavigationButton"]
            || [[[view class] description] isEqualToString:@"UINavigationItemButtonView"]
            || [[[view class] description] isEqualToString:@"UIButton"])
        {
            view.frame = CGRectMake(view.frame.origin.x, 5.0, view.frame.size.width, 34.0);
        }

        if (IS_IOS_6)
        {
            if ([[[view class] description] isEqualToString:@"UINavigationItemView"])
            {
                view.frame = CGRectMake(view.frame.origin.x, 13.0, view.frame.size.width, view.frame.size.height);
            }
        }
    }
}


@end
