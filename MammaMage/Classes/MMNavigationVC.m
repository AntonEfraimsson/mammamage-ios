//
//  MMNavigationVC.m
//  MammaMage
//
//  Created by Narek Haytyan on 11/03/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "MMNavigationVC.h"

@interface MMNavigationVC ()

@end

@implementation MMNavigationVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
