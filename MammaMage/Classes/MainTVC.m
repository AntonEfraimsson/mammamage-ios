//
//  MainTVC.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/24/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "MainTVC.h"

#import "ToggleViewController.h"

#import "DBLevel.h"

@interface MainTVC ()

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (strong, nonatomic) UIFont *buttonFont;
@property (strong, nonatomic) UIFont *buttonFontBig;

// menu buttons
@property (weak, nonatomic) IBOutlet UIButton *infoMenuButton;
@property (weak, nonatomic) IBOutlet UIButton *tutorialMenuButton;
@property (weak, nonatomic) IBOutlet UIButton *levelsMenuButton;
@property (weak, nonatomic) IBOutlet UIButton *pelvisMenuButton;
@property (weak, nonatomic) IBOutlet UIButton *everydayMenuButton;

//Toggle view
@property (strong, nonatomic) ToggleViewController *toggleView;
@end

@implementation MainTVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // creating new back button with an empty title
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc] initWithTitle:@" "
                                                                style:UIBarButtonItemStylePlain
                                                               target:nil
                                                               action:nil];
    self.navigationItem.backBarButtonItem = backBtn;
	
    
    // Do any additional setup after loading the view.
    self.buttonFont = [UIFont fontWithName:FONT_BOLD size:17];
    self.buttonFontBig = [UIFont fontWithName:FONT_BOLD size:21];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"startimage_iphone5.jpg"]];
    backgroundImageView.contentMode = UIViewContentModeTopRight;
    self.tableView.backgroundView = backgroundImageView;
    
    self.contentLabel.font = [UIFont fontWithName:FONT_NORMAL size:self.contentLabel.font.pointSize];
    
    [self customizeMenuButtonFonts];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    //Localize Menu Text
    
    [self.contentLabel setText: NSLocalizedString(@"menu_content_label", nil)];
    
    [self.infoMenuButton setTitle: NSLocalizedString(@"menu_info_button", nil) forState:UIControlStateNormal];
    [self.tutorialMenuButton setTitle: NSLocalizedString(@"menu_tutorial_button", nil) forState:UIControlStateNormal];
    [self.levelsMenuButton setTitle: NSLocalizedString(@"menu_levels_button", nil) forState:UIControlStateNormal];
    [self.pelvisMenuButton setTitle: NSLocalizedString(@"menu_pelvis_button", nil) forState:UIControlStateNormal];
    [self.everydayMenuButton setTitle: NSLocalizedString(@"menu_everyday_button", nil) forState:UIControlStateNormal];
    
    //Toggle Message
    
    self.view.userInteractionEnabled = NO;
    self.toggleView = [[ToggleViewController alloc] initWithNibName:@"ToggleView" bundle:[NSBundle mainBundle]];
    self.toggleView.type = kToggleViewTypeOneButton;
    self.toggleView.parentView = self.view;
    self.toggleView.isBold = NO;
    [self.toggleView setMessage: NSLocalizedString(@"main_screen_toggle_message", nil) ];
    ((UIView*)self.toggleView.view.subviews[0]).alpha = 0.7;
    self.toggleView.view.alpha = 0;
    
    [APP_DELEGATE.window addSubview:self.toggleView.view];
    
    self.toggleView.view.center = self.view.center;
    [UIView animateWithDuration:0.3 animations:^{
        self.toggleView.view.alpha = 1;
    } completion:^(BOOL finished) {
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[DBAdapter sharedInstance] checkForDatabaseMigration];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    static UIView *statusBarView = nil;
    if (!statusBarView)
    {
        statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
        statusBarView.backgroundColor = [UIColor blackColor];
    }
    
    if (IS_IOS_7) {
        self.view.y = 20;
        [statusBarView removeFromSuperview];
        [APP_DELEGATE.window addSubview:statusBarView];
    }
}

- (void)customizeMenuButtonFonts
{
    // customizing 1st button
    self.infoMenuButton.titleLabel.font = self.buttonFont;
    
    // customizing middle buttons
    // 2nd button
    self.tutorialMenuButton.titleLabel.font = self.buttonFont;
    
    // 3rd buttons
    self.levelsMenuButton.titleLabel.font = self.buttonFontBig;
    
    // 4th button
    self.pelvisMenuButton.titleLabel.font = self.buttonFont;
    // end customizing middle buttons
    
    // customizing last button
    self.everydayMenuButton.titleLabel.font = self.buttonFont;
}

#pragma mark - Acitons

- (IBAction)openWebsiteTapped:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://mammamage.se/kontakt/"]];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
}

@end
