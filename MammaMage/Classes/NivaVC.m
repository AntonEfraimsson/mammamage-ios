//
//  ExerciseListTVC.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/27/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "NivaVC.h"
#import "ExerciseCell.h"
#import "LevelInfoCell.h"
#import "EGODatabase.h"
#import "DBExercise.h"
#import "DBLevel.h"
#import "ExcerciseVC.h"
#import <MediaPlayer/MediaPlayer.h>

@interface NivaVC ()

@property (strong, nonatomic) NSNumber *totalCompletedExercisesCount;

@property (strong, nonatomic) DBLevel *levelDB;
@property (strong, nonatomic) DBExercise *exerciseDB;
@property (strong, nonatomic) EGODatabaseResult *exercises;
@property (strong, nonatomic) EGODatabaseResult *levels;
@property (strong, nonatomic) EGODatabaseRow *level;

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *levelTestButton;

@end


@implementation NivaVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadData];
    
    // creating new back button with an empty title
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc] initWithTitle:@" "
                                                                style:UIBarButtonItemStylePlain
                                                               target:nil
                                                               action:nil];
    self.navigationItem.backBarButtonItem = backBtn;

    self.navigationItem.title = [self.level stringForColumn:NSLocalizedString(@"name", nil)];
    /* L0gg3r */
    //self.navigationItem.title = [self.level stringForColumn:@"caption"];
    /*L0gg3r finish*/

    // unhiding navigation bar
    [self.navigationController setNavigationBarHidden:NO];
    
    // setting background image
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[@"bg_level" stringByAppendingString:[NSString stringWithFormat:@"%d.jpg", [self.levelId integerValue]]]]];
    
    [self.levelTestButton setTitle:NSLocalizedString(@"perform_test", nil) forState:UIControlStateNormal];
    
    self.levelTestButton.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:self.levelTestButton.titleLabel.font.pointSize];
    self.backButton.titleLabel.font = self.levelTestButton.titleLabel.font;
    
    if (IS_IOS_7) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadData];
    [self.tableView reloadData];
    
    

    
    NSInteger levelProgress = [[self.levelDB getRemainingStepsCountForLevelId:self.levelId] integerValue];
    NSLog(@"levelProgress %d",levelProgress);
    NSLog(@"intForColumn: %d",[self.level intForColumn:@"completed"] );
    
    
    //Always enable test button
    [self.levelTestButton setEnabled:YES];
    
    
    if (levelProgress < 1) {
        [self.levelTestButton setEnabled:YES];
    } else {
       
        if ([LOCK_ALL_LEVELS isEqualToString:@"YES"]) {
             [self.levelTestButton setEnabled:NO];
        } else {
             [self.levelTestButton setEnabled:YES];
        }
    }
    
    
    if ([self.level intForColumn:@"completed"] == 1 && levelProgress < 1) {
        [self.levelTestButton setEnabled:YES];
//        self.levelTestButton.hidden = YES;
//        self.backButton.hidden = NO;
    } else {
        self.levelTestButton.hidden = NO;
        self.backButton.hidden = YES;
    }
}

- (void)loadData
{
    self.levelDB = [DBLevel sharedInstance];
    self.exerciseDB = [DBExercise sharedInstance];
    self.exercises = [self.exerciseDB selectExercisesForLevel:self.levelId];
    self.levels = [self.levelDB selectLevels:self.levelId];
    self.level = [self.levels rowAtIndex:0];
    
    self.totalCompletedExercisesCount = [self.levelDB getTotalCompletedExercisesCountForLevelId:self.levelId];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 50;
            break;
        case 1:
            return 0;
            break;
    }
    return 0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0) {
        return 1;
    }
    return self.exercises.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    
    // 1st section
    if(indexPath.section == 0) {
    
        // Homan
        // This text is not dynamic anymore
        
        CellIdentifier = @"LevelProgressCell";
        LevelInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
        cell.captionLabelText = NSLocalizedString(@"level_advisory_caption", nil);

       // cell.captionLabelText = [self.level stringForColumn:@"caption"];
       //cell.footerLabelText = [self.level stringForColumn:@"footer"];

        NSMutableAttributedString *bufferStr;
     //   UIFont* bold13 = [UIFont fontWithName:FONT_NORMAL size:13];
         NSString *descText = NSLocalizedString(@"level_advisory_text", nil);
        bufferStr = [NSMutableAttributedString attributedStringWithString: descText];
        [bufferStr setFont:[UIFont fontWithName:FONT_NORMAL size:13]];
        
//        [bufferStr setFont:bold13 range: [buf rangeOfString:@"Kontrollera att ryggen är stilla!"]];
//        [bufferStr setTextColor:[UIColor darkTextColor]];
        cell.descriptionLabelText = bufferStr;
      
        
        //end
        
        if ([self.level intForColumn:@"completed"] != 0) {
            [cell updateProgressIndicator:100 forLevel:self.levelId];
            cell.remainingStepsCountText = [NSString stringWithFormat:@"%d", -1];
        } else {
            NSInteger levelProgress = [[self.levelDB getRemainingStepsCountForLevelId:self.levelId] integerValue];
            double percent = 100 - 100 * (levelProgress) / 10;
            
            [cell updateProgressIndicator:percent forLevel:self.levelId];
            cell.remainingStepsCountText = [NSString stringWithFormat:@"%d", levelProgress];
        }
        
        if ([self.totalCompletedExercisesCount intValue] < 3)
        {
            [cell updateStarProgress:0 forLevel:self.levelId];
        }
        else if ([self.totalCompletedExercisesCount intValue] < 6)
        {
            [cell updateStarProgress:45 forLevel:self.levelId];
        }
        else
        {
            [cell updateStarProgress:100 forLevel:self.levelId];
        }

        return cell;
    }

    // 2nd section
    CellIdentifier = @"Cell";
    ExerciseCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.exerciseNumber = indexPath.row + 1;
    cell.exerciseName = [[self.exercises rowAtIndex:indexPath.row] stringForColumn:NSLocalizedString(@"caption", nil)];
    if (indexPath.row == 0) {
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_exercise_top_normal.png"]];
        cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_exercise_top_highlight.png"]];
    } else if (indexPath.row == self.exercises.count - 1) {
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_exercise_bottom_normal.png"]];
        cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_exercise_bottom_highlight.png"]];
    } else {
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_exercise_middle_normal.png"]];
        cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_exercise_middle_highlight.png"]];
    }
    

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
         // Homan
         // This text is not dynamic anymore
         // NSString *description = [self.level stringForColumn:@"description"];
//        CGFloat newHeight = [description sizeWithFont:[UIFont fontWithName:FONT_NORMAL size:13]
//                                    constrainedToSize:CGSizeMake(270, 1000)
//                                        lineBreakMode:UILineBreakModeWordWrap].height;
        return 280;//IS_IOS_7 ? 155 + 55: 185 + 55;
    }
    
    return 53;
}

- (IBAction)backButtonPressed:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString:@"showSingleExerciseSegue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        NSNumber *selectedExerciseId = [[self.exercises rowAtIndex:indexPath.row] numberForColumn:@"id"];
        
        [segue.destinationViewController setExerciseId:selectedExerciseId];
        [segue.destinationViewController setIsTest:NO];
    } else if ([segue.identifier isEqualToString:@"showTestExerciseSegue"]) {
        [segue.destinationViewController setIsTest:YES];
    }
    [segue.destinationViewController setLevelId:self.levelId];
}

@end
