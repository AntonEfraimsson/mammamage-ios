//
//  NormalLabel.m
//  MammaMage
//
//  Created by Davit Sahakyan on 12/5/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "NormalLabel.h"

@implementation NormalLabel

- (void) awakeFromNib
{
    self.font = [UIFont fontWithName:FONT_NORMAL
                                size:self.font.pointSize];
    self.textColor = [UIColor whiteColor];
}

@end
