//
//  MammaMagePelvisVC.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/24/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "OmMusklernaVC.h"

@interface OmMusklernaVC () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation OmMusklernaVC

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view.
    // creating new back button with an empty title
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc] initWithTitle:@" "
                                                                style:UIBarButtonItemStylePlain
                                                               target:nil
                                                               action:nil];
    self.navigationItem.backBarButtonItem = backBtn;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.jpg"]];
    
    NSURL *URL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"OmMusklerna" ofType:@"html"]];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:URL];
    [self.webView loadRequest:requestObj];
    self.webView.backgroundColor = [UIColor clearColor];
    self.webView.opaque = NO;
    self.webView.scrollView.bounces = NO;
    self.webView.delegate = self;
    
    //Set localized title
    [self.navigationItem setTitle: NSLocalizedString(@"menu_pelvis_button", nil)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // unhiding navigation bar
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([request.URL.absoluteString hasPrefix:@"http"]) {
        [[UIApplication sharedApplication] openURL:request.URL];
        return NO;
    }
    return YES;
}

- (void)dealloc
{
    self.webView.delegate = nil;
}

@end
