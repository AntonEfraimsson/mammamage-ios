//
//  MammaMageTutorialVC.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/24/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "OmTrainingVC.h"
#import "Constants.h"
#import "DDPageControl.h"

@interface OmTrainingVC () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView1;
@property (weak, nonatomic) IBOutlet UIScrollView *mainVerticalScroll;

@property (weak, nonatomic) IBOutlet DDPageControl *pageControl;
@property (weak, nonatomic) IBOutlet DDPageControl *pageControl1;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (IBAction)changePage:(DDPageControl *)sender;
- (IBAction)changePage2:(DDPageControl *)sender;

@end

@implementation OmTrainingVC

- (int)screenResulution
{
    return [UIScreen mainScreen].bounds.size.width;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view.
    // creating new back button with an empty title
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc] initWithTitle:@" "
                                                                style:UIBarButtonItemStylePlain
                                                               target:nil
                                                               action:nil];
    self.navigationItem.backBarButtonItem = backBtn;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.jpg"]];
    
//    self.scrollView.contentSize = self.scrollView.frame.size;
//    self.scrollView1.contentSize = self.scrollView1.frame.size;
//    
////    DLog(@" ------------------- %g, %g", self.scrollView.contentSize.width, self.scrollView.contentSize.height);
//
//    CGRect frame = self.scrollView.frame;
//    frame.size.width = [self screenResulution];
//    self.scrollView.frame = frame;
//
//    frame = self.scrollView1.frame;
//    frame.size.width = [self screenResulution];
//    self.scrollView1.frame = frame;
//    
////    DLog(@"frame %@",NSStringFromCGRect([self.scrollView.subviews[0] frame]));
//    
//    self.mainVerticalScroll.contentSize = self.mainVerticalScroll.frame.size;
//    frame = [UIScreen mainScreen].bounds;
//    frame.size.height -= 64;
//    self.mainVerticalScroll.frame = frame;
//    
//    [self.pageControl setNumberOfPages:3];
//    [self.pageControl setIndicatorDiameter:7];
//    [self.pageControl setOnColor:[UIColor whiteColor]];
//    [self.pageControl setOffColor:[UIColor darkGrayColor]];
//
//    [self.pageControl1 setNumberOfPages:4];
//    [self.pageControl1 setIndicatorDiameter:7];
//    [self.pageControl1 setOnColor:[UIColor whiteColor]];
//    [self.pageControl1 setOffColor:[UIColor darkGrayColor]];
    
//    NSURL *URL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"page_2" ofType:@"html"]];
    

    //NSURL *URL = [[NSBundle mainBundle] URLForResource:@"OmTraning.html" withExtension:nil];
    NSURL *URL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"OmTraning" ofType:@"html"]];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:URL];
    
    [self.webView loadRequest:requestObj];
    self.webView.backgroundColor = [UIColor clearColor];
    self.webView.opaque = NO;
    self.webView.scrollView.bounces = NO;
    self.webView.delegate = self;
    
    //Set localized title
    [self.navigationItem setTitle: NSLocalizedString(@"menu_tutorial_button", nil)];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([request.URL.absoluteString hasPrefix:@"http"]) {
        [[UIApplication sharedApplication] openURL:request.URL];
        return NO;
    }
    return YES;
}

- (void)dealloc
{
    self.mainVerticalScroll.delegate = nil;
    self.scrollView.delegate = nil;
    self.scrollView1.delegate = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // unhiding navigation bar
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

// Scroll View Delegates
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int resulution = [UIScreen mainScreen].bounds.size.width;
    int page = scrollView.contentOffset.x/resulution;
    if (scrollView == self.scrollView) {
        [self.pageControl setCurrentPage:page];
    } else if (scrollView == self.scrollView1) {
        [self.pageControl1 setCurrentPage:page];
    }
}

// Page Controll
- (IBAction)changePage:(DDPageControl *)sender
{
    int resolution = [UIScreen mainScreen].bounds.size.width;
    int page =  sender.currentPage;
    CGPoint offset = self.scrollView.contentOffset;
    
    offset.x = page * resolution;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.scrollView.contentOffset = offset;
    }];
}

- (IBAction)changePage2:(DDPageControl *)sender
{
    int resolution = [UIScreen mainScreen].bounds.size.width;
    int page =  sender.currentPage;
    CGPoint offset = self.scrollView1.contentOffset;
    
    offset.x = page * resolution;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.scrollView1.contentOffset = offset;
    }];
}
@end