//
//  ToggleViewController.h
//  MammaMage
//
//  Created by Davit Sahakyan on 11/20/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

typedef enum {
    kToggleViewTypeOneButton = 1,
    kToggleViewTypeDoubleButton
} ToggleViewType;

#import <UIKit/UIKit.h>

@interface ToggleViewController : UIViewController
- (void) setMessage:(NSString*) text;
@property (nonatomic) ToggleViewType type;
@property (nonatomic) BOOL isBold;
@property (weak , nonatomic) UIView *parentView;

@end
