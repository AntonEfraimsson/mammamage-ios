//
//  ToggleViewController.m
//  MammaMage
//
//  Created by Davit Sahakyan on 11/20/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "ToggleViewController.h"

@interface ToggleViewController ()

@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIImageView *popUpMessage;
@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet UIButton *yesButton;

@end

@implementation ToggleViewController

- (void)setMessage:(NSString *)text
{
    self.textLabel.text = text;
    
    if (!self.isBold) {
        self.textLabel.font = [UIFont fontWithName:FONT_NORMAL size:13];
    }
    
    CGSize tempsize = [text sizeWithFont:self.textLabel.font constrainedToSize:CGSizeMake(240, 999) lineBreakMode:UILineBreakModeWordWrap];
    
    CGRect tempframe = self.textLabel.frame;
    tempframe.size = tempsize;
    tempframe.origin.y = 100;
    self.textLabel.frame = tempframe;
    self.textLabel.center = CGPointMake(self.view.center.x, self.view.center.y - 30);
    self.textLabel.textAlignment = UITextAlignmentLeft;
    
    tempsize.width += 50;
    tempsize.width = MAX(tempsize.width, 290);
    tempsize.height += 50 + self.yesButton.frame.size.height;
    
    tempframe = self.popUpMessage.frame;
    tempframe.size = tempsize;
    self.popUpMessage.frame=tempframe;
    self.popUpMessage.center = self.view.center;
    
    
    
    if (self.type == kToggleViewTypeDoubleButton) {
        tempframe = self.noButton.frame;
        tempframe.origin.y = self.textLabel.frame.origin.y + self.textLabel.frame.size.height + 10;
        self.noButton.frame = tempframe;
        
        tempframe = self.yesButton.frame;
        tempframe.origin.y = self.noButton.frame.origin.y;
        self.yesButton.frame = tempframe;
    } else if (self.type  == kToggleViewTypeOneButton) {
        tempframe = self.yesButton.frame;
        tempframe.size.width = 240;
        [self.yesButton setBackgroundImage:[[self.yesButton backgroundImageForState:UIControlStateNormal] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 30, 0, 30)] forState:UIControlStateNormal];
        [self.yesButton setBackgroundImage:[[self.yesButton backgroundImageForState:UIControlStateHighlighted] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 30, 0, 30)] forState:UIControlStateHighlighted];
        tempframe.origin.y = self.textLabel.frame.origin.y + self.textLabel.frame.size.height + 10;
        self.yesButton.frame = tempframe ;
        CGPoint point = self.yesButton.center;
        
        point.x = self.yesButton.superview.center.x;
        self.yesButton.center = point;
        
        self.noButton.hidden = YES;
        [self.yesButton setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateNormal];  //NSLocalizedString(@"OK", nil);
    }
    
}

- (IBAction)togglebuttonPressed:(UIButton *)sender
{
    if (self.type == kToggleViewTypeDoubleButton) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"aboutToggleState" object:@(sender.tag)];
    }
    else {
        self.parentView.userInteractionEnabled = YES ; 
        [UIView animateWithDuration:0.3 animations:^{
            self.view.alpha = 0;
        } completion:^(BOOL finished) {
            [self.view removeFromSuperview];
        }];

    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.textLabel = self.view.subviews[2];
        self.noButton = self.view.subviews[3];
        self.yesButton = self.view.subviews[4];
        self.popUpMessage = self.view.subviews[1];
        self.popUpMessage.image = [[UIImage imageNamed:@"popup_bg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(50, 50, 50, 50)];
        
        self.isBold = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	if (self.type == kToggleViewTypeOneButton){
        self.view.userInteractionEnabled = YES;
    }
}

@end
