//
//  ToggleViewControllerDone.h
//  MammaMage
//
//  Created by Homan Farhadian on 1/9/13.
//  Copyright (c) 2013 SOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToggleViewControllerDone : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bg;

@end
