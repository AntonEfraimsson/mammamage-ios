//
//  ToggleViewControllerDone.m
//  MammaMage
//
//  Created by Homan Farhadian on 1/9/13.
//  Copyright (c) 2013 SOS. All rights reserved.
//

#import "ToggleViewControllerDone.h"

@interface ToggleViewControllerDone ()

@end

@implementation ToggleViewControllerDone

- (IBAction)okTouched:(UIButton *)sender {
    
    self.parentViewController.view.userInteractionEnabled = YES ;
    [UIView animateWithDuration:0.3 animations:^{
        self.view.alpha = 0;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}

@end
