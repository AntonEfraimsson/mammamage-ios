//
//  ToggleViewControllerNiva.h
//  MammaMage
//
//  Created by Davit Sahakyan on 12/6/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToggleViewControllerNiva : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *bg;
@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@end
