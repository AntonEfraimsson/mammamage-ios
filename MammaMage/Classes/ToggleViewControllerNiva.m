//
//  ToggleViewControllerNiva.m
//  MammaMage
//
//  Created by Davit Sahakyan on 12/6/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "ToggleViewControllerNiva.h"

@interface ToggleViewControllerNiva ()

@property (strong, nonatomic) IBOutlet UITextView *textView;

@end

@implementation ToggleViewControllerNiva

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

    return self;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    self.textView.selectable = NO;
    self.textView.editable = NO;
    self.textView.textContainer.lineFragmentPadding = 0;
    self.textView.textContainerInset = UIEdgeInsetsZero;

    [self.view.subviews.firstObject setAlpha:0.7];
    self.view.backgroundColor = [UIColor clearColor];
    self.bg.image = [self.bg.image resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)];
    
    [self.yesButton setBackgroundImage:[[self.yesButton backgroundImageForState:UIControlStateNormal] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 30, 0, 30)] forState:UIControlStateNormal];
    [self.yesButton setBackgroundImage:[[self.yesButton backgroundImageForState:UIControlStateHighlighted] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 30, 0, 30)] forState:UIControlStateHighlighted];

    
    NSString *text = NSLocalizedString(@"exercise_popup_text", nil);
    self.textView.attributedText = [self formatString:text];
}

- (NSAttributedString *)formatString:(NSString *)text
{
    NSMutableString *t = [text mutableCopy];
    NSMutableArray *rangesArray = [NSMutableArray new];
    while (t.length) {
        NSRange startRange = [t rangeOfString:@"<b>"];
        if (startRange.location == NSNotFound) {
            break;
        }
        [t deleteCharactersInRange:startRange];
        
        NSRange endRange = [t rangeOfString:@"</b>"];
        
        NSRange resRange = NSMakeRange(startRange.location, endRange.location - startRange.location);
        [rangesArray addObject:[NSValue valueWithRange:resRange]];
        [t deleteCharactersInRange:endRange];
    }
    
    NSMutableAttributedString *result = [[NSMutableAttributedString alloc] initWithString:t];
    [result addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"GillSans" size:13] range:NSMakeRange(0, t.length-1)];
    for (NSValue *v in rangesArray) {
        NSRange range = [v rangeValue];
        NSString *subString = [t substringWithRange:range];
        NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"0123456789:-"];
        if ([set isSupersetOfSet:[NSCharacterSet characterSetWithCharactersInString:subString]]) {
            [result addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:@"GillSans-Bold" size:13]
                           range:range];
        } else {
            [result addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:@"GillSans-Bold" size:18]
                           range:range];
        }
    }
    
    return result;
}

- (IBAction)okTouched:(UIButton *)sender {
    
    self.parentViewController.view.userInteractionEnabled = YES ;
    [UIView animateWithDuration:0.3 animations:^{
        self.view.alpha = 0;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}

@end
