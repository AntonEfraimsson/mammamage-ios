//
//  ToggleViewControllerTest.h
//  MammaMage
//
//  Created by Davit Sahakyan on 12/6/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExcerciseVC.h"

@interface ToggleViewControllerTest : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *bg;
@property (nonatomic,weak) ExcerciseVC* delegate;
@property (weak, nonatomic) IBOutlet UITextView *failedTextView;
@property (weak, nonatomic) IBOutlet UITextView *approvedTextView;
@property (strong, nonatomic) IBOutlet UIView *secondAlertView;
@end
