//
//  ToggleViewControllerTest.m
//  MammaMage
//
//  Created by Davit Sahakyan on 12/6/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "ToggleViewControllerTest.h"
@class ExcerciseVC;
@interface ToggleViewControllerTest ()

@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet UILabel *captionLabel;
@property (weak, nonatomic) IBOutlet UILabel *approvedLabel;
@property (weak, nonatomic) IBOutlet UILabel *failedLabel;

- (IBAction)didFinishToggle:(UIButton *)sender;

@end

@implementation ToggleViewControllerTest
@synthesize approvedTextView = _approvedTextView;
@synthesize failedTextView = _failedTextView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIFont *buttonFont = [UIFont fontWithName:FONT_BOLD size:16];
    self.yesButton.titleLabel.font = buttonFont;
    self.noButton.titleLabel.font = buttonFont;
    

    [self.yesButton setTitle:NSLocalizedString(@"button_yes", nil) forState:UIControlStateNormal];
    [self.noButton setTitle:NSLocalizedString(@"button_no", nil) forState:UIControlStateNormal];
    [self.captionLabel setText:NSLocalizedString(@"test_completed_caption", nil)];
    [self.approvedLabel setText:NSLocalizedString(@"test_approved", nil)];
    [self.failedLabel setText:NSLocalizedString(@"test_failed", nil)];
}

- (IBAction)didFinishToggle:(UIButton *)sender {
    
    if (0 == sender.tag) {
        //No Pressed
        [self.delegate didFinishTestToggle:NO];
            self.secondAlertView.hidden = NO;
    }
    else if (1 == sender.tag) {
        //Yes Pressed
        [self.delegate didFinishTestToggle:YES];
        self.secondAlertView.hidden = NO;
    }
}

@end
