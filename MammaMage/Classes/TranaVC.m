//
//  LevelsTVC.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/24/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "TranaVC.h"

#import "EGODatabase.h"
#import <Foundation/Foundation.h>
#import "DBLevel.h"
#import "NivaVC.h"
#import "LevelCell.h"


@interface TranaVC ()

@property (strong, nonatomic) EGODatabaseResult *data;

// level colors
@property (strong, nonatomic) NSArray *levelColors;

@end

@implementation TranaVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // creating new back button with an empty title
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc] initWithTitle:@" "
                                                                style:UIBarButtonItemStylePlain
                                                               target:nil
                                                               action:nil];
    self.navigationItem.backBarButtonItem = backBtn;
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.jpg"]];
    if (IS_IOS_7) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    //Set localized title
    [self.navigationItem setTitle: NSLocalizedString(@"menu_levels_button", nil)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // unhiding navigation bar
    [self.navigationController setNavigationBarHidden:NO animated:YES];

    [self prepareDataForTable];
}

- (void)prepareDataForTable
{
    self.data = [[DBLevel sharedInstance] selectLevels:nil];
    [self.tableView reloadData];
}

#pragma mark - Table view controller delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    LevelCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    NSInteger levelNumber = indexPath.row + 1;
    if(indexPath.row == self.data.count-1) {
        cell.isLast = YES;
    } else {
        cell.isLast = NO;
    }
    
    [cell setLevelCaption:NSLocalizedString(@"level_caption", nil)];
    
    [cell setLevelDescription:[[self.data rowAtIndex:indexPath.row] stringForColumn: NSLocalizedString(@"description", nil)]];
    [cell setLevelNumber:[UIImage imageNamed:[@"nr_level" stringByAppendingString:[NSString stringWithFormat:@"%d.png", levelNumber]]]];
    [cell setArrowImage:[UIImage imageNamed:[@"arrow_level" stringByAppendingString:[NSString stringWithFormat:@"%d.png", levelNumber]]]];

    if ([[self.data rowAtIndex:indexPath.row] intForColumn:@"accessible"] == 0) {
        if ([LOCK_ALL_LEVELS isEqualToString:@"YES"]) {
            [cell disableCellWithIndex:(indexPath.row)];
        } else {
            [cell enableCellWithIndex:(indexPath.row)];
        }
        
    } else {
        [cell enableCellWithIndex:(indexPath.row)];
    }
    
    NSNumber *totalCompletedExercizesForLevel = [[DBLevel sharedInstance] getTotalCompletedExercisesCountForLevelId:@(levelNumber)];

    if ([totalCompletedExercizesForLevel intValue] < 3)
    {
        [cell updateStarProgress:0 forLevel:@(levelNumber)];
    }
    else if ([totalCompletedExercizesForLevel intValue] < 6)
    {
        [cell updateStarProgress:45 forLevel:@(levelNumber)];
    }
    else
    {
        [cell updateStarProgress:100 forLevel:@(levelNumber)];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 91;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showExercisesSegue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        NSNumber *selectedLevelId = [[self.data rowAtIndex:indexPath.row] numberForColumn:@"id"];
        [segue.destinationViewController setLevelId:selectedLevelId];
    }
}

@end
