//
//  UIViewController+BackSwipe.m
//  MammaMage
//
//  Created by David Sahakyan on 3/12/14.
//  Copyright (c) 2014 SOS. All rights reserved.
//

#import "UIViewController+BackSwipe.h"

@interface UIViewController()
@end

@implementation UIViewController (BackSwipe)

- (void)viewDidLoad
{
    if (IS_IOS_7) {
        self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

@end
