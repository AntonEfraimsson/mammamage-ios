//
//  Util.h
//  PTPlus
//
//  Created by Ashot Tonoyan on 28/05/2012.
//  Copyright (c) 2012 SocialObjects Software. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>

@interface Utils : NSObject

+ (BOOL)validateEmail:(NSString *)candidate;
+ (void) createEditableCopyOfResourceIfNeeded:(NSString *)fileName;
+ (void)createEditableCopyOfResourceForce:(NSString *)fileName;
+ (NSString *)secondsToTimerString:(CGFloat) timeInSeconds withHours:(BOOL) hours;
+ (NSString *)cleanYouTubeURL:(NSString *) originalURL;
+ (NSString *)trim:(NSString *) sourceString;
+ (BOOL)isValidEmail:(NSString *) checkString;
+ (UIColor* )colorWithHex:(NSString* )hex;
+ (UIColor* )colorWithHex:(NSString* )hex alpha:(CGFloat )alpha;

+ (void)saveToDefaultsWithKey:(NSString *) key object:(id) object;
+ (id)readFromDefaultsWithKey:(NSString *) key;
+ (void)removeFromDefaultsWithKey:(NSString *) key;

//+ (void)simpleAlertWithTitle:(NSString *) title message:(NSString *) message;

+ (NSString *)documentsPathForFileName:(NSString *) name;

//+ (UIImage *)imageByScalingAndCroppingForSize:(CGSize)targetSize sourceImage:(UIImage *) sourceImage;
//+ (BOOL)savePhoto:(UIImage *)image withFileName:(NSString *)fileName;

+ (id)nullIfEmpty:(id)object;
+ (id)emptyIfNull:(id)object;

#pragma mark - File system utils
+ (void)cleanupDocmentsDirectory;

#pragma mark - UI Utility methods
+ (NSString *)backButtonDefaultTitle:(UIViewController *) controller;
+ (void)makeBackButtonUppercase:(UIViewController *) controller;
+ (UIImageView *)tableCellBackgroundViewTop;
+ (UIImageView *)tableCellBackgroundViewMiddle;
+ (UIImageView *)tableCellBackgroundViewBottom;
+ (UIImageView *)tableCellBackgroundViewSingle;

+ (void)showIndicator;
+ (void)hideIndicator;
+ (void)showToggle;

#pragma mark - Facebook Utils
+ (void)shareToFBImage:(UIImage *)image message:(NSString *)message complete:(void(^)(id result,NSError* error))callback;
+ (void)likeFacebookObjectWithId:(NSString *)fbObjectId withComplete:(void(^)(id result,NSError* eror))callback;
+ (void)unlikeFacebookObjectWithId:(NSString *)fbObjectId withComplete:(void(^)(id result,NSError* error))callback;
+ (void)getFBFriendsComplete:(void (^)(id result, NSError *error))callback;
+ (void)getFBPlacesComplete:(void (^)(id result, NSError *error))callback;
+ (void)getFBPrivacyFriendlistsComplete:(void (^)(id, NSError *))callback;
+ (void)getFBPlaceByName:(NSString *)name withComplete:(void (^)(id result, NSError *error))callback;




@end
