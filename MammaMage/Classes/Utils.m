//
//  Util.m
//  PTPlus
//
//  Created by Ashot Tonoyan on 28/05/2012.
//  Copyright (c) 2012 SocialObjects Software. All rights reserved.
//
#define INDICATOR_TAG 234
#define TOGGLE_TAG 235

#import "Utils.h"
#import "EGODatabase.h"

@implementation Utils

+ (BOOL)validateEmail:(NSString *)candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate];
}

+ (UIColor*)colorWithHex:(NSString*)hex
{
    return [Utils colorWithHex:hex alpha:1];
}

+ (UIColor*)colorWithHex:(NSString*)hex alpha:(CGFloat)alpha {
    
    assert(7 == [hex length]);
    assert('#' == [hex characterAtIndex:0]);
    NSString *redHex = [NSString stringWithFormat:@"0x%@", [hex substringWithRange:NSMakeRange(1, 2)]];
    NSString *greenHex = [NSString stringWithFormat:@"0x%@", [hex substringWithRange:NSMakeRange(3, 2)]];
    NSString *blueHex = [NSString stringWithFormat:@"0x%@", [hex substringWithRange:NSMakeRange(5, 2)]];
    unsigned redInt = 0;
    NSScanner *rScanner = [NSScanner scannerWithString:redHex];
    [rScanner scanHexInt:&redInt];
    unsigned greenInt = 0;
    NSScanner *gScanner = [NSScanner scannerWithString:greenHex];
    [gScanner scanHexInt:&greenInt];
    unsigned blueInt = 0;
    NSScanner *bScanner = [NSScanner scannerWithString:blueHex];
    [bScanner scanHexInt:&blueInt];
    return [UIColor colorWithRed:(redInt/255.0) green:(greenInt/255.0) blue:(blueInt/255.0) alpha:alpha];
}

+ (void)createEditableCopyOfResourceIfNeeded:(NSString *)fileName
{
    // First, test for existence.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSLog(documentsDirectory);
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    success = [fileManager fileExistsAtPath:writableDBPath];
    if (success) return;
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}

+ (void)createEditableCopyOfResourceForce:(NSString *)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:fileName];

    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
    
    BOOL success = [fileManager removeItemAtPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to delete existing database file with message '%@'.", [error localizedDescription]);
    }
    
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}

+ (NSString *)secondsToTimerString:(CGFloat) timeInSeconds withHours:(BOOL) withHours
{
    CGFloat remainder = timeInSeconds;
    NSInteger hours = (remainder < 0) ? ceil(remainder / 3600.0) : floor(remainder / 3600.0);
    remainder = remainder - (3600.0 * hours);
    
    NSInteger minutes = (remainder < 0) ? ceil(remainder / 60.0) : floor(remainder / 60.0);
    remainder = remainder - (60.0 * minutes);
    
    NSInteger seconds = remainder;
    remainder = remainder - seconds;
    
    NSInteger milliseconds = remainder * 100.0;
    
    NSString *output = (remainder < 0) ? @"-" : @"";
    
    if (withHours) {
        return [output stringByAppendingFormat:@"%d:%.2d:%.2d", abs(hours), abs(minutes), abs(seconds)];
    } else {
        return [output stringByAppendingFormat:@"%d:%.2d.%.2d", abs(minutes), abs(seconds), abs(milliseconds)];
    }
}

+ (NSString *)cleanYouTubeURL:(NSString *) originalURL
{
    NSURL *urlObject = [NSURL URLWithString:originalURL];
    NSMutableDictionary *queryComponents = [[NSMutableDictionary alloc] init];
    for (NSString *queryComponent in [urlObject.query componentsSeparatedByString:@"&"]) {
        NSArray *component = [queryComponent componentsSeparatedByString:@"="];
        if ([component count] == 2) {
            [queryComponents setObject:[component objectAtIndex:1] forKey:[component objectAtIndex:0]];
        }
    }
    if ([queryComponents objectForKey:@"v"]) {
        return [NSString stringWithFormat:@"http://www.youtube.com/v/%@", [queryComponents objectForKey:@"v"]];
    }
    return nil;
}

+ (NSString *)trim:(NSString *) sourceString
{
    return [sourceString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (BOOL)isValidEmail:(NSString *) checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (void)saveToDefaultsWithKey:(NSString *) key object:(id) object
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:object forKey:key];
    [defaults synchronize];
}


+ (id)readFromDefaultsWithKey:(NSString *) key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:key];
}

+ (void)removeFromDefaultsWithKey:(NSString *) key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:key];
    [defaults synchronize];
}





+ (NSString *)documentsPathForFileName:(NSString *) name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    return [documentsPath stringByAppendingPathComponent:name];
}

/*
 + (UIImage *)imageByScalingAndCroppingForSize:(CGSize)targetSize sourceImage:(UIImage *) sourceImage
 {
 CGFloat scaleFactor = 0.0;
 CGFloat scaledWidth = targetSize.width;
 CGFloat scaledHeight = targetSize.height;
 CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
 
 if (CGSizeEqualToSize(sourceImage.size, targetSize) == NO)
 {
 CGFloat widthFactor = targetSize.width / sourceImage.size.width;
 CGFloat heightFactor = targetSize.height / sourceImage.size.height;
 
 if (widthFactor > heightFactor) {
 scaleFactor = widthFactor; // scale to fit height
 } else {
 scaleFactor = heightFactor; // scale to fit width
 }
 scaledWidth  = sourceImage.size.width * scaleFactor;
 scaledHeight = sourceImage.size.height * scaleFactor;
 
 // center the image
 if (widthFactor > heightFactor) {
 thumbnailPoint.y = (targetSize.height - scaledHeight) * 0.5;
 } else if (widthFactor < heightFactor) {
 thumbnailPoint.x = (targetSize.width - scaledWidth) * 0.5;
 }
 }
 
 UIGraphicsBeginImageContext(targetSize); // this will crop
 
 CGRect thumbnailRect = CGRectMake(thumbnailPoint.x, thumbnailPoint.y, scaledWidth, scaledHeight);
 [sourceImage drawInRect:thumbnailRect];
 
 UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
 if(newImage == nil) {
 DLog(@"could not scale image");
 }
 
 UIGraphicsEndImageContext(); //pop the context to get back to the default
 return newImage;
 }
 
 + (BOOL)savePhoto:(UIImage *)image withFileName:(NSString*)fileName
 {
 BOOL photoSaved = [UIImageJPEGRepresentation(image, 1) writeToFile:fileName atomically:YES];
 CGSize thumbSize = CGSizeMake(PHOTO_THUMBNAIL_WIDTH_HEIGHT, PHOTO_THUMBNAIL_WIDTH_HEIGHT);
 if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
 thumbSize.width *= [[UIScreen mainScreen] scale];
 thumbSize.height *= [[UIScreen mainScreen] scale];
 }
 UIImage *thumbImage = [Utils imageByScalingAndCroppingForSize:thumbSize sourceImage:image];
 NSString *thumbFileName = [fileName stringByAppendingString:@".thumb"];
 BOOL thumbSaved = [UIImageJPEGRepresentation(thumbImage, 1) writeToFile:thumbFileName atomically:YES];
 return (photoSaved && thumbSaved);
 }*/

+ (id)nullIfEmpty:(id)object {
    if (object) {
        return object;
    } else {
        return [NSNull null];
    }
}

+ (id)emptyIfNull:(id)object {
    if (!object) {
        return @"";
    } else if ([object isEqual:[NSNull null]]) {
        return @"";
    }
    
    return object;
}

#pragma mark - File system utils

+ (void)cleanupDocmentsDirectory
{
    return;
    // Path to the Documents directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    if ([paths count] > 0)
    {
        NSError *error = nil;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Print out the path to verify we are in the right place
        NSString *directory = [paths objectAtIndex:0];
        NSLog(@"Directory: %@", directory);
        
        // For each file in the directory, create full path and delete the file
        for (NSString *file in [fileManager contentsOfDirectoryAtPath:directory error:&error])
        {
            NSString *filePath = [directory stringByAppendingPathComponent:file];
            NSLog(@"File : %@", filePath);
            
            BOOL fileDeleted = [fileManager removeItemAtPath:filePath error:&error];
            
            if (fileDeleted != YES || error != nil)
            {
                // Deal with the error...
                //                DLog(@"error");
            }
        }
        
    }
}





#pragma mark - UI Utility methods

+ (NSString *)backButtonDefaultTitle:(UIViewController *) controller
{
    return [[[controller.navigationController.childViewControllers objectAtIndex:[controller.navigationController.childViewControllers count] - 1] navigationItem] title];
}

+ (void)makeBackButtonUppercase:(UIViewController *)controller {
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:[[Utils backButtonDefaultTitle:controller] uppercaseString]
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target: nil
                                                                     action: nil];
    controller.navigationItem.backBarButtonItem = newBackButton;
}

+ (UIImageView *)tableCellBackgroundViewTop {
    return [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"field_input_top.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(20.0, 20.0, 20.0, 20.0)]];
}

+ (UIImageView *)tableCellBackgroundViewMiddle {
    return [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"field_input_middle.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(20.0, 20.0, 20.0, 20.0)]];
}

+ (UIImageView *)tableCellBackgroundViewBottom {
    return [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"field_input_bottom.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(20.0, 20.0, 20.0, 20.0)]];
}

+ (UIImageView *)tableCellBackgroundViewSingle {
    return [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"field_input.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(20.0, 20.0, 20.0, 20.0)]];
}

+ (void)showIndicator
{
    UIWindow *mainWindow = [[UIApplication sharedApplication].delegate window];
    
    if(![mainWindow viewWithTag:INDICATOR_TAG]) {
        UIView *activityView = [[[NSBundle mainBundle] loadNibNamed:@"AlertView" owner:self options:nil] objectAtIndex:1];
        //activityView.layer.cornerRadius = 10;
        activityView.tag = INDICATOR_TAG;
        activityView.center = mainWindow.center;
        [[[UIApplication sharedApplication].delegate window] addSubview:activityView];
    }
    
    [mainWindow setUserInteractionEnabled:NO];
    [[mainWindow viewWithTag:INDICATOR_TAG] setHidden:NO];
    //    [(UIActivityIndicatorView*)[[[mainWindow viewWithTag:INDICATOR_TAG] subviews] lastObject] startAnimating];
}

+ (void)hideIndicator
{
    [[[[UIApplication sharedApplication].delegate window] viewWithTag:INDICATOR_TAG] setHidden:YES];
    [[[UIApplication sharedApplication].delegate window] setUserInteractionEnabled:YES];
}

+ (void)showToggle
{
    UIWindow *mainWindow = [[UIApplication sharedApplication].delegate window];
    
    if(![mainWindow viewWithTag:TOGGLE_TAG]) {
        UIView *activityView = [[[NSBundle mainBundle] loadNibNamed:@"ToggleView" owner:self options:nil] objectAtIndex:0];
        
        activityView.tag = TOGGLE_TAG;
        activityView.center = mainWindow.center;
        [[[UIApplication sharedApplication].delegate window] addSubview:activityView];
    }
    
    [[mainWindow viewWithTag:TOGGLE_TAG] setHidden:NO];
    //    [(UIActivityIndicatorView*)[[[mainWindow viewWithTag:INDICATOR_TAG] subviews] lastObject] startAnimating];
}

#pragma mark - Facebook Utils
+ (void)shareToFBImage:(UIImage *)image message:(NSString *)message complete:(void(^)(id result,NSError* error))callback
{
    if ([FBSession activeSession].state == FBSessionStateCreatedTokenLoaded)
    {
        [FBSession openActiveSessionWithAllowLoginUI:NO];
    }
    else if ([FBSession activeSession].state == FBSessionStateClosed || ![FBSession activeSession].accessTokenData.accessToken)
    {
        NSError *error = [NSError errorWithDomain:nil code:0 userInfo:@{@"info":@"notLogined"}];
        callback(nil,error);
        return;
    }
    UIImage *imgSource = image;
    NSString *strMessage = message;
    NSMutableDictionary* photosParams = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                         imgSource,@"source",
                                         strMessage,@"message",
                                         nil];
    [FBRequestConnection startWithGraphPath:@"me/photos" parameters:photosParams HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         callback(result,error);
     }];
}

+ (void)likeFacebookObjectWithId:(NSString *)fbObjectId withComplete:(void(^)(id result,NSError* error))callback
{
    if ([FBSession activeSession].state == FBSessionStateCreatedTokenLoaded)
    {
        [FBSession openActiveSessionWithAllowLoginUI:NO];
    }
    else if ([FBSession activeSession].state == FBSessionStateClosed || ![FBSession activeSession].accessTokenData.accessToken)
    {
        NSError *error = [NSError errorWithDomain:nil code:0 userInfo:@{@"info":@"notLogined"}];
        callback(nil,error);
        return;
    }
    
    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"/%@/likes",fbObjectId] parameters:@{} HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         callback(result,error);
     }];
}

+ (void)unlikeFacebookObjectWithId:(NSString *)fbObjectId withComplete:(void(^)(id result,NSError* error))callback
{
    if ([FBSession activeSession].state == FBSessionStateCreatedTokenLoaded)
    {
        [FBSession openActiveSessionWithAllowLoginUI:NO];
    }
    else if ([FBSession activeSession].state == FBSessionStateClosed || ![FBSession activeSession].accessTokenData.accessToken)
    {
        NSError *error = [NSError errorWithDomain:nil code:0 userInfo:@{@"info":@"notLogined"}];
        callback(nil,error);
        return;
    }
    
    
    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"/%@/likes",fbObjectId] parameters:@{} HTTPMethod:@"DELETE"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         callback(result,error);
     }];
}

+ (void)getFBFriendsComplete:(void (^)(id, NSError *))callback
{
    if (![FBSession activeSession].isOpen) {
        [FBSession openActiveSessionWithAllowLoginUI: NO];
    }
    
    NSString *query =  @"select uid, name, is_app_user "
    @"from user "
    @"where uid in (select uid2 from friend where uid1=me() )";
    NSDictionary *queryParam =
    [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    // Make the API request that uses FQL
    [FBRequestConnection startWithGraphPath:@"/fql"
                                 parameters:queryParam
                                 HTTPMethod:@"GET"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error) {
                              if (callback)
                                  callback(result,error);
                          }];
}

+ (void)getFBPrivacyFriendlistsComplete:(void (^)(id, NSError *))callback
{
    if (![FBSession activeSession].isOpen) {
        [FBSession openActiveSessionWithAllowLoginUI: NO];
    }
    
    NSString *query =  @"SELECT flid,name,type from friendlist  where owner=me()";
    NSDictionary *queryParam =
    [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    // Make the API request that uses FQL
    [FBRequestConnection startWithGraphPath:@"/fql"
                                 parameters:queryParam
                                 HTTPMethod:@"GET"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error) {
                              if (callback)
                                  callback(result,error);
                          }];
}

+ (void)getFBPlacesComplete:(void (^)(id, NSError *))callback
{
    if (![FBSession activeSession].isOpen) {
        [FBSession openActiveSessionWithAllowLoginUI: NO];
    }
    
    NSString *query =  @"select page_id,name, location from page where page_id in (select target_id FROM checkin WHERE author_uid=me())";
    NSDictionary *queryParam =
    [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    // Make the API request that uses FQL
    [FBRequestConnection startWithGraphPath:@"/fql"
                                 parameters:queryParam
                                 HTTPMethod:@"GET"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error) {
                              if (callback)
                                  callback(result,error);
                          }];
}

+ (void)getFBPlaceByName:(NSString *)name withComplete:(void (^)(id result, NSError *error))callback
{
    if (![FBSession activeSession].isOpen) {
        [FBSession openActiveSessionWithAllowLoginUI: NO];
    }
    
    NSString *query =  [NSString stringWithFormat:@"SELECT page_id, name, location from page where name = \"%@\"",name];
    NSDictionary *queryParam =
    [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    // Make the API request that uses FQL
    [FBRequestConnection startWithGraphPath:@"/fql"
                                 parameters:queryParam
                                 HTTPMethod:@"GET"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error) {
                              if (callback)
                                  callback(result,error);
                          }];
}

@end
