//
//  MammaMageInfoVC.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/24/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "VadArVC.h"
#import "OHAttributedLabel.h"
#import "NSAttributedString+Attributes.h"

@interface VadArVC () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *topContentLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomContentLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (weak, nonatomic) IBOutlet OHAttributedLabel *attributedLabel;
@property (weak, nonatomic) IBOutlet OHAttributedLabel *attributedLinkLabel;


@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation VadArVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    CGSize size = self.contentScrollView.contentSize;
//    size.height = 20.0;
//    for(UIView *subView in self.contentScrollView.subviews) {
//        size.height += subView.bounds.size.height;
//    }
//    self.contentScrollView.contentSize = size;
//    
//    UIFont *contentFont = [UIFont fontWithName:FONT_NORMAL size:self.topContentLabel.font.pointSize];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.jpg"]];
    
//    // styling content
//    self.topContentLabel.font = contentFont;
//    self.bottomContentLabel.font = contentFont;
//    
//    
//    self.contentScrollView.contentSize = CGSizeMake(self.contentScrollView.frame.size.width, self.contentScrollView.frame.size.height);
//    self.contentScrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 44);
//    
//    UILabel *label = self.contentScrollView.subviews[0];
//    label.font = [UIFont fontWithName:FONT_ITALIC size:label.font.pointSize];
//    
//    UIFont *boldFont = [UIFont fontWithName:FONT_BOLD size:self.attributedLabel.font.pointSize];
//    UIFont *regularFont = [UIFont fontWithName:FONT_NORMAL size:self.attributedLabel.font.pointSize];
//    NSString *mainText = self.attributedLabel.text;
//    NSMutableAttributedString *attrString = [NSMutableAttributedString attributedStringWithString:self.attributedLabel.text];
//    [attrString setTextColor:[UIColor whiteColor]];
//    [attrString setFont:regularFont];
//    [attrString setFont:boldFont range: [mainText rangeOfString:@"Uppgift: återfå elasticitet, hjälpa muskeln att dra ihop sig"]];
//    [attrString setFont:boldFont range: [mainText rangeOfString:@"Uppgift: skapa läkning"]];
//    [attrString setFont:boldFont range: [mainText rangeOfString:@"Uppgift: lära om: att magen ska aktiveras INÅT"]];
//    self.attributedLabel.attributedText = attrString;
//    
//    if (IS_IOS_7)
//        self.contentScrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 20, 0);
    
    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"VadAr.html" withExtension:nil];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:URL];
    
    [self.webView loadRequest:requestObj];
    self.webView.backgroundColor = [UIColor clearColor];
    self.webView.opaque = NO;
    self.webView.delegate = self;
    self.webView.scrollView.bounces = NO;
    
    //Set localized title
    [self.navigationItem setTitle: NSLocalizedString(@"menu_info_button", nil)];
}

- (IBAction)openLink:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://mammamage.se"]];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([request.URL.absoluteString hasPrefix:@"http"]) {
        [[UIApplication sharedApplication] openURL:request.URL];
        return NO;
    }
    return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)dealloc
{
    self.contentScrollView.delegate = nil;
}

@end
