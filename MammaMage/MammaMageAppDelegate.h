//
//  MammaMageAppDelegate.h
//  MammaMage
//
//  Created by Narek Haytyan on 10/24/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

// git https://github.com/HomanS/MammaMage.git

#import <UIKit/UIKit.h>

@interface MammaMageAppDelegate : UIResponder <UIApplicationDelegate>
 
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) BOOL didNotified;
@end
