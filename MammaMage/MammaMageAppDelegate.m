//
//  MammaMageAppDelegate.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/24/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import "MMNavigationVC.h"
#import "MammaMageAppDelegate.h"
//#import "TestFlight.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@implementation MammaMageAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    //#ifdef DEBUG
    //#pragma clang diagnostic push
    //#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    //    [TestFlight setDeviceIdentifier:[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
    //#pragma clang diagnostic pop
    //#endif
    //
    //    // Testflight init
    //    [TestFlight takeOff:@"8e74241eb1eb993f09b9238e98367aac_ODM1NjkyMDEyLTA1LTAzIDEwOjM0OjI4LjY1MTI1MQ"];
    
//    [TestFlight takeOff:@"194da8d1-d891-42ca-b854-36a6abd6141a"];
    
    [Fabric with:@[[Crashlytics class]]];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    [Utils createEditableCopyOfResourceIfNeeded:@"MammaMage.sqlite"];
    
    [self customizeNavigationBar];
    
    UILocalNotification *not;
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showDialog" object:nil];
    
    return YES;
}

- (void)customizeNavigationBar
{
    UIImage *navBarbackground = [UIImage imageNamed:@"top_bar.png"];
    
    // styling navigation bar
    [[UINavigationBar appearanceWhenContainedIn:[MMNavigationVC class], nil] setBackgroundImage:navBarbackground forBarMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor blackColor], UITextAttributeTextColor,
      [UIColor whiteColor], UITextAttributeTextShadowColor,
      [NSValue valueWithUIOffset:UIOffsetMake(0, 2)], UITextAttributeTextShadowOffset,
      [UIFont fontWithName:FONT_BOLD size:18], UITextAttributeFont, nil]];
    
    
    // styling back button
    UIImage *normalImage = [UIImage imageNamed:@"btn_back_normal.png"];
    UIImage *highlitedImage = [UIImage imageNamed:@"btn_back_highlight.png"];

    if (IS_IOS_6) {
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:[normalImage
                                                                    resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]
                                                          forState:UIControlStateNormal
                                                        barMetrics:UIBarMetricsDefault];
        
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:[highlitedImage
                                                                    resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]
                                                          forState:UIControlStateHighlighted
                                                        barMetrics:UIBarMetricsDefault];
    } else {
        if ([UINavigationBar instancesRespondToSelector:@selector(setBackIndicatorImage:)]) {
            [[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"transparent_1px"]];
            [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"transparent_1px"]];
        }
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:[normalImage
                                                                    resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 26)]
                                                          forState:UIControlStateNormal
                                                        barMetrics:UIBarMetricsDefault];

        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:[highlitedImage
                                                                    resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 26)]
                                                          forState:UIControlStateHighlighted
                                                        barMetrics:UIBarMetricsDefault];
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    self.didNotified = NO;
}

@end
