            var xDown = null,                                                        
                yDown = null,                                                        
                index = 0,
                slider1,
                slider2,
                slids1,
                slids2,
                dots1,
                dots2;

              var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;       
            function handleTouchStart(evt) {                                         
                xDown = evt.touches[0].clientX;                                      
                yDown = evt.touches[0].clientY;                                      
            };                                                

            function handleTouchMove(evt, slids, slider, dots) {
                if ( ! xDown || ! yDown ) {
                    return;
                }

                var xUp = evt.touches[0].clientX;                                    
                var yUp = evt.touches[0].clientY;

                var xDiff = xDown - xUp;
                var yDiff = yDown - yUp;

                if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {
                    if ( xDiff > 0 ) {
                        if(index == slids.length - 1) return;
                        var nextSlide = ++index;
                        var x = -1 * width * nextSlide + 'px';
                        slider.style.WebkitTransform = 'translate3d(' + x + ', 0, 0)';
                        for (var i = 0; i < dots.length; i++) {
                            dots[i].removeAttribute('class');
                        };
                        dots[nextSlide].setAttribute('class','active');

                    } else {
                        if(index == 0) return;
                        var prevtSlide = --index;
                        var x = -1 * width * prevtSlide + 'px';
                        slider.style.WebkitTransform= 'translate3d(' + x + ', 0, 0)';
                        
                        for (var i = 0; i < dots.length; i++) {
                            dots[i].removeAttribute('class');
                        };
                        dots[prevtSlide].setAttribute('class','active');
                    }                       
                } 
                /* reset values */
                xDown = null;
                yDown = null;                                             
            };

            
            window.onload = function(){
                slider1 = document.getElementById('image_slider_1');
                slider1.style.width = width * 3 + 'px';
                slids1 = document.querySelectorAll("ul#image_slider_1 li");
                dots1  = document.querySelectorAll("ul#pager_1 li");
                for (var i = 0; i < slids1.length; i++) {
                    slids1[i].style.width = width + 'px';
                };
                slider1.addEventListener('touchstart', handleTouchStart, false);        
                slider1.addEventListener('touchmove', function(e){handleTouchMove(e, slids1, slider1, dots1)}, false);   

                slider2 = document.getElementById('image_slider_2');
                slider2.style.width = width * 3 + 'px';
                slids2 = document.querySelectorAll("ul#image_slider_2 li");
                dots2  = document.querySelectorAll("ul#pager_2 li");
                for (var i = 0; i < slids2.length; i++) {
                    slids2[i].style.width = width + 'px';
                };

                slider2.addEventListener('touchstart', handleTouchStart, false);        
                slider2.addEventListener('touchmove', function(e){handleTouchMove(e, slids2, slider2, dots2)} , false);   
            }        