  var xDown = null,                                                        
                yDown = null,                                                        
                index = 0,
                slider,
                slids,
                dots;

              var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;       
            function handleTouchStart(evt) {                                         
                xDown = evt.touches[0].clientX;                                      
                yDown = evt.touches[0].clientY;                                      
            };                                                

            function handleTouchMove(evt) {
                if ( ! xDown || ! yDown ) {
                    return;
                }

                var xUp = evt.touches[0].clientX;                                    
                var yUp = evt.touches[0].clientY;

                var xDiff = xDown - xUp;
                var yDiff = yDown - yUp;

                if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {
                    if ( xDiff > 0 ) {
                        if(index == slids.length - 1) return;
                        var nextSlide = ++index;
                        var x = -1 * width * nextSlide + 'px';
                        slider.style.WebkitTransform = 'translate3d(' + x + ', 0, 0)';
                        for (var i = 0; i < dots.length; i++) {
                            dots[i].removeAttribute('class');
                        };
                        dots[nextSlide].setAttribute('class','active');

                    } else {
                        if(index == 0) return;
                        var prevtSlide = --index;
                        var x = -1 * width * prevtSlide + 'px';
                        slider.style.WebkitTransform= 'translate3d(' + x + ', 0, 0)';
                        
                        for (var i = 0; i < dots.length; i++) {
                            dots[i].removeAttribute('class');
                        };
                        dots[prevtSlide].setAttribute('class','active');
                    }                       
                } 
                /* reset values */
                xDown = null;
                yDown = null;                                             
            };