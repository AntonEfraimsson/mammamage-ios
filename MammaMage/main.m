//
//  main.m
//  MammaMage
//
//  Created by Narek Haytyan on 10/24/12.
//  Copyright (c) 2012 SOS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MammaMageAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MammaMageAppDelegate class]));
    }
}
